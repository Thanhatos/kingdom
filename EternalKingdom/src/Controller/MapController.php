<?php

// src/Controller/MapController.php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MapController extends AbstractController
{
    /**
     * @Route("/map")
     */
    public function Map(): Response
    {
        return $this->render('Map/map.html.twig');
    }

    /**
     * Display the server selection
     * @Route("/map/choose")
     */
    public function ServerSelection(): Response
    {
        // TODO Load available servers from DB relevant for the current user (difficulty / language preference?)
        // Server Object : ID, name, CustomOwner, current, maxCapacity, difficulty

        return $this->render('Game/ServerSelection.html.twig');
    }
}
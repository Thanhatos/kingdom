<?php

// src/Controller/DefaultController.php
namespace App\Controller;

use App\Entity\Lord;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * Index can return the default page, the server selection or the management page.
     * @Route("/", name="homepage")
     */
    public function Index(): Response
    {
        $isConnected = true;

        if($isConnected)
        {
            // TODO Load proper UserID from connected player

            // For now, always fallback to the first lord id from the DB and if nothing is in the DB display the server selection
            $lordRepo = $this->getDoctrine()->getRepository(Lord::class);
            $lord = $lordRepo->findOneBy([]);

            $hasGameInProgress = $lord !== null;

            if($hasGameInProgress)
            {
                $parameters = [
                    'id' => $lord->getId()
                ];

                return $this->forward('App\Controller\ManagementController::Management', $parameters);
            }
            else
            {
                return $this->forward('App\Controller\MapController::ServerSelection');
            }
        }

        return $this->render('index.html.twig');
    }
}
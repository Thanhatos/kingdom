<?php

// src/Controller/CityController.php
namespace App\Controller;

use App\Entity\Lord;
use App\Model\SwapCandidate;
use App\Model\Tile;
use App\Service\GridManager;
use App\Service\CityManager;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/*
 * Conseil from Nawadoo
 * Côté serveur il "suffit" d'envisager tous les cas de figure (paramètre manquant, paramètre non numérique, etc)
 * Et veiller à ne pas avoir de paramètres contradictoires. Par exemple pour vérifier un stock il est inutile de transmettre le montant en paramètre, puisqu'on va le récupérer côté serveur de toutes façons
 * C'est un peu toujours les mêmes contrôles
 * Si tu veux commencer doucement, tu peux faire un envoi de formulaire classique qui actualise toute la page, à l'ancienne. Et dans un second temps, remplacer ça par de l'ajax
 */

/**
 * CityController will display the City page
 * It permits to access the Match3 to increase resource stocks & population
 * and permits to make new buildings and improve the army.
 * @package App\Controller
 */
class CityController extends AbstractController
{
    //Managers
    private $gridManager;
    private $cityManager;

    /**
     * CityController constructor.
     * @param GridManager $gridManager
     * @param CityManager $cityManager
     */
    public function __construct(GridManager $gridManager, CityManager $cityManager)
    {
        $this->gridManager = $gridManager;
        $this->cityManager = $cityManager;
    }

    /**
     * City index page
     * @Route("/city/{id}")
     * @param int $id
     * @return Response
     */
    public function city(int $id): Response
    {
        return $this->GenerateCommonCityPageResponse($id, 'City/index.html.twig');
    }

    /**
     * @Route("/city/{id}/chooseConstruction")
     * @param int $id
     * @return Response
     */
    public function chooseConstruction(int $id): Response
    {
        $lordRepository = $this->GetDoctrine()->GetRepository(Lord::class);
        $lord = $lordRepository->find($id);
        $this->InitCityGrid($lord);
        $allParameters = [
            'lord' => $lord,
            'maxAmounts' => $this->cityManager->getMaxStorages($lord->getCity()),
            'constructionProjects' =>$this->cityManager->getDisplayedProjects($lord->getCity())
        ];
        return $this->render('City/optionChooseBuilding.html.twig', $allParameters);
    }

     /**
     * Request needs the constructionName !
     * @Route("/city/{id}/startConstruction/{name}_{level}")
     * @param int $id
     * @param string $name
     * @param int $level
     * @return Response
     */
    public function startConstruction(int $id, string $name, int $level): Response
    {
        // TODO Verify if the logged player owns this lord!
        $entityManager = $this->getDoctrine()->getManager();
        $lordRepository = $entityManager->GetRepository(Lord::class);
        $lord = $lordRepository->find($id);

        if ($this->cityManager->startConstruction($lord->getCity(), $name, $level))
        {
            $entityManager->flush();
            return $this->redirectToRoute("app_city_city", ['id' => $id]);
        }

        return $this->redirectToRoute("app_city_chooseconstruction", ['id' => $id]);
    }

    /**
     * @Route("/city/{id}/cancelConstruction")
     * @param int $id
     * @return Response
     */
    public function cancelConstruction(int $id): Response
    {
        // TODO Verify if the logged player owns this lord!
        $entityManager = $this->getDoctrine()->getManager();
        $lordRepository = $entityManager->GetRepository(Lord::class);
        $lord = $lordRepository->find($id);

        if ($this->cityManager->cancelConstruction($lord->getCity()))
        {
            $entityManager->flush();
        }
        return $this->redirectToRoute("app_city_city", ['id' => $id]);
    }

    /**
     * @Route("/city/{id}/chooseSoldierUpgrade")
     * @param int $id
     * @return Response
     */
    public function chooseSoldierUpgrade(int $id): Response
    {
        $lordRepository = $this->GetDoctrine()->GetRepository(Lord::class);
        $lord = $lordRepository->find($id);
        $this->InitCityGrid($lord);
        $allParameters = [
            'lord' => $lord,
            'maxAmounts' => $this->cityManager->getMaxStorages($lord->getCity()),
            'upgradeSchemas' => $this->cityManager->getDisplayedConversionSchemas($lord->getCity(), 'army')
        ];
        return $this->render('City/optionUpgradeSoldier.html.twig', $allParameters);
    }

    /**
     * Request needs the producedResourceName !
     * @Route("/city/{id}/upgradeSoldier/{resourceName}", methods={"POST"}))
     * @param Request $request
     * @param int $id
     * @param string $resourceName
     * @return Response
     */
    public function upgradeSoldier(Request $request, int $id, string $resourceName): Response
    {
        // TODO Verify if the logged player owns this lord!
        $entityManager = $this->getDoctrine()->getManager();
        $lordRepository = $entityManager->GetRepository(Lord::class);
        $lord = $lordRepository->find($id);
        $amount = $request->request->get('count');

        if ($this->cityManager->convertResources($lord->getCity(), $resourceName, $amount))
        {
            $entityManager->flush();
        }
        return $this->redirectToRoute("app_city_choosesoldierupgrade", ['id' => $id]);
    }



    /**
     * Request needs the producedResourceName !
     * @Route("/city/{id}/toggleRecruitment"))
     * @param int $id
     * @return Response
     */
    public function toggleRecruitment(int $id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $lordRepository = $entityManager->GetRepository(Lord::class);
        $lord = $lordRepository->find($id);
        $this->cityManager->toggleRecruitment($lord->getCity());
        $entityManager->flush();
        return $this->redirectToRoute("app_city_choosesoldierupgrade", ['id' => $id]);
    }

    /**
     * @Route("/city/{id}/chooseResourceConversion")
     * @param int $id
     * @return Response
     */
    public function chooseResourceConversion(int $id): Response
    {
        $lordRepository = $this->GetDoctrine()->GetRepository(Lord::class);
        $lord = $lordRepository->find($id);
        $this->InitCityGrid($lord);

        $allParameters = [
            'lord' => $lord,
            'maxAmounts' => $this->cityManager->getMaxStorages($lord->getCity()),
            'conversionSchemas' =>$this->cityManager->getDisplayedConversionSchemas($lord->getCity(), 'resource')
        ];
        return $this->render('City/optionConvertResources.html.twig', $allParameters);
    }

    /**
     * Request needs the producedResourceName !
     * @Route("/city/{id}/convertResources/{resourceName}", methods={"POST"}))
     * @param Request $request
     * @param int $id
     * @param string $resourceName
     * @return Response
     */
    public function convertResources(Request $request, int $id, string $resourceName): Response
    {
        // TODO Verify if the logged player owns this lord!
        $entityManager = $this->getDoctrine()->getManager();
        $lordRepository = $entityManager->GetRepository(Lord::class);
        $lord = $lordRepository->find($id);
        $amount = $request->request->get('count');

        if ($this->cityManager->convertResources($lord->getCity(), $resourceName, $amount))
        {
            $entityManager->flush();
        }

        return $this->redirectToRoute("app_city_chooseresourceconversion", ['id' => $id]);
    }

    /**
     * @Route("/city/{id}/chooseGeneralName")
     * @param int $id
     * @return Response
     */
    public function chooseGeneralName(int $id): Response
    {
        return $this->GenerateCommonCityPageResponse($id, 'City/optionRecruitGeneral.html.twig');
    }

    /**
     * Request needs the producedResourceName !
     * @Route("/city/{id}/recruitGeneral", methods={"POST"}))
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function recruitGeneral(Request $request, int $id): Response
    {
        // TODO Verify if the logged player owns this lord!
        $entityManager = $this->getDoctrine()->getManager();
        $lordRepository = $entityManager->GetRepository(Lord::class);
        $lord = $lordRepository->find($id);
        $name = $request->request->get('name');

        if ($this->cityManager->recruitGeneral($lord->getCity(), $name))
        {
            $entityManager->flush();
            return $this->redirectToRoute("app_map_map", ['id' => $id]);
        }
        return $this->redirectToRoute("app_city_city", ['id' => $id]);   
    }


    /**
     * Request needs the CitizenId !
     * @Route("/city/{id}/convertCitizen", methods={"POST"})
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function ConvertCitizen(Request $request, int $id): Response
    {
        $response = new Response();
        $statusCode = Response::HTTP_FORBIDDEN;

        // TODO Verify if the logged player owns this lord!
        $entityManager = $this->getDoctrine()->getManager();
        $lordRepository = $entityManager->GetRepository(Lord::class);
        $lord = $lordRepository->find($id);

        $payload = json_decode($request->request->get("json"), true);
        $citizenId = $payload['citizenId'];

        if ($this->cityManager->convertCitizen($lord->getCity(), $citizenId))
        {
            $statusCode = Response::HTTP_OK;
            $entityManager->flush();
        }

        $response->setStatusCode($statusCode);

        return $response;
    }

    /**
     * Request needs the swap coordinates from client JSON paylood !
     * @Route("/city/{id}/swapTiles", methods={"POST"})
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function swapTiles(Request $request, int $id): Response
    {
        $response = new Response();
        $statusCode = Response::HTTP_FORBIDDEN;

        $entityManager = $this->getDoctrine()->getManager();
        $lordRepository = $entityManager->GetRepository(Lord::class);
        $lord = $lordRepository->find($id);
        $payload = json_decode($request->request->get("json"), true);

        $tile1 = new Tile($payload['r1'], $payload['c1']);
        $tile2 = new Tile($payload['r2'], $payload['c2']);
        $swapCandidate = new SwapCandidate($tile1, $tile2);
        if ($this->gridManager->swap($lord->getCityRef(), $swapCandidate))
        {
            $statusCode = Response::HTTP_OK;

            //if the grid has been reset, $steps contains also a string of the new grid
            $steps = $this->gridManager->getChainReactionSteps();

            $this->cityManager->convertAlignments($lord->getCityRef(), $steps);
            $entityManager->flush();

            // Send all resources data updated (all for now)
            $resources = [];
            foreach($lord->getCity()->getStorages()->getIterator() as $i => $item)
            {
                $resourceInfo = array(
                    "name" => $item->getResource()->getName(),
                    "amount" => $item->getAmount()
                );
                array_push($resources, $resourceInfo);
            }
            $resourceInfo = array(
                "name" => "turn",
                "amount" => $lord->getCity()->getRemainingTurns(),
            );
            array_push($resources, $resourceInfo);

            $clientPayload = array();
            $clientPayload['steps'] = $steps;
            $clientPayload['resources'] = $resources;

            $response->setContent(json_encode($clientPayload));
        }

        $response->setStatusCode($statusCode);

        return $response;
    }

    /**
     * Generate a common city page response
     * @param int $id Lord ID
     * @param string $view Template to display
     * @param array $parameters Extra parameters to load with the template
     * @return Response
     */
    private function GenerateCommonCityPageResponse(int $id, string $view, array $parameters = []): Response
    {
        // TODO Generate the array parameter!
        // TODO Create a Player/City Object and add Armies and City Logs Object

        $lordRepository = $this->GetDoctrine()->GetRepository(Lord::class);
        $lord = $lordRepository->find($id);
        $this->InitCityGrid($lord);
        $commonParameters = [
            'lord' => $lord,
            'maxAmounts' => $this->cityManager->getMaxStorages($lord->getCity()),
        ];
        $allParameters = array_merge($commonParameters, $parameters);

        return $this->render($view, $allParameters);
    }

    /**
     * Initializes city's grid & persist it
     * @param Lord $lord
     */
    private function InitCityGrid(Lord $lord)
    {
        if (empty($lord->getCity()->getGrid()))
        {
            $grid = $this->gridManager->initGrid($lord->getCity());
            $lord->getCity()->setGrid(GridManager::convertGridToString($grid));
            $this->GetDoctrine()->getManager()->flush();
        }
    }

}
<?php

// src/Controller/ManagementController.php
namespace App\Controller;

use App\Entity\Lord;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * ManagementController will display the Management page
 * Inspect how you are, your cities, your relations with other realms, your productions, ...
 * @package App\Controller
 */
class ManagementController extends AbstractController
{
    /**
     * Lord Management page
     * Forwarded from DefaultController when accessing index webpage.
     * @Route("/management/{id}")
     *
     * @param int $id LordId
     * @return Response
     */
    public function Management(int $id): Response
    {
        $lordRepository = $lord = $this->GetDoctrine()->GetRepository(Lord::class);
        $lord = $lordRepository->find($id);

        return $this->render(
            'Management/management.html.twig',
            [
                'lord' => $lord
            ]
        );
    }
}
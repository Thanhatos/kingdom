<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GeneralController extends AbstractController
{
    /**
     * Display the server selection
     * @Route("/general/{id}/units")
     * @param int $id
     * @return Response
     */
    public function TransferUnits(int $id): Response
    {
        return $this->render('General/transferUnits.html.twig');
    }
}
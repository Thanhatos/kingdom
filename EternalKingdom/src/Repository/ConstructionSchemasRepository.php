<?php

namespace App\Repository;

use App\Entity\ConstructionSchemas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ConstructionSchemas|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConstructionSchemas|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConstructionSchemas[]    findAll()
 * @method ConstructionSchemas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConstructionSchemasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConstructionSchemas::class);
    }

    // /**
    //  * @return ConstructionSchemas[] Returns an array of ConstructionSchemas objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ConstructionSchemas
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

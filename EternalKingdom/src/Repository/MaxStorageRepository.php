<?php

namespace App\Repository;

use App\Entity\MaxStorage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MaxStorage|null find($id, $lockMode = null, $lockVersion = null)
 * @method MaxStorage|null findOneBy(array $criteria, array $orderBy = null)
 * @method MaxStorage[]    findAll()
 * @method MaxStorage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MaxStorageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MaxStorage::class);
    }

    // /**
    //  * @return MaxStorage[] Returns an array of MaxStorage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MaxStorage
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

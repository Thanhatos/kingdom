<?php

namespace App\DataFixtures;

use App\Entity\ResourceConversionSchemas;
use App\Service\GameDataService;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Output\ConsoleOutput;


class ResourceConversionSchemaFixtures extends Fixture implements FixtureGroupInterface, DependentFixtureInterface
{
    public $conversionSchemas = [];

    private $manager;
    private $output;
    private $gameDataService;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->output = new ConsoleOutput();
        $this->gameDataService = new GameDataService($entityManager);
    }

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $this->gameDataService->loadDicts();

        $this->fillData();

        foreach ($this->conversionSchemas as $SchemaData)
        {
            [$constructionDict, $resourceDict] = [$this->gameDataService->getConstructionDict(), $this->gameDataService->getResourceDict()];
            $resourceConversionSchemas = new resourceConversionSchemas();
            
            $resourceConversionSchemas->setRequiredBuilding($constructionDict[$SchemaData['construction']]);
            $resourceConversionSchemas->setRequiredLevel($SchemaData['level']);
            $resourceConversionSchemas->setRequiredResource($resourceDict[$SchemaData['requiredResource']]);
            $resourceConversionSchemas->setRequiredAmount($SchemaData['requiredAmount']);
            $resourceConversionSchemas->setproducedResource($resourceDict[$SchemaData['producedResource']]);
            $resourceConversionSchemas->setproducedAmount($SchemaData['producedAmount']);
            $manager->persist($resourceConversionSchemas);
        }
        $manager->flush();
    }

    //Determines conversion schemas from level
    private function fillData()
    {
        $cauldronEfficiency = [100, 80, 70, 60, 50]; $factorybutcherEfficiency = [5, 7, 10, 15, 20];
        for($level=1; $level<6; $level++)
        {
            $factorySchema = ['construction' => 'factory', 'level' => $level, 'requiredResource' => 'lin', 'requiredAmount' => 1, 
                              'producedResource' => 'gold', 'producedAmount' => $factorybutcherEfficiency[$level-1]];
            $butcherSchema = ['construction' => 'butcher', 'level' => $level, 'requiredResource' => 'horse', 'requiredAmount' => 1, 
                              'producedResource' => 'wheat', 'producedAmount' => $factorybutcherEfficiency[$level-1]];
            $cauldronSchema = ['construction' => 'cauldron', 'level' => $level, 'requiredResource' => 'wood', 'requiredAmount' => $cauldronEfficiency[$level-1], 
                              'producedResource' => 'iron', 'producedAmount' => 1];
            $archerSchema1 = ['construction' => 'archery', 'level' => $level, 'requiredResource' => 'soldier', 'requiredAmount' => 1, 
                             'producedResource' => 'archer', 'producedAmount' => 1];
            $archerSchema2 = ['construction' => 'archery', 'level' => $level, 'requiredResource' => 'lin', 'requiredAmount' => 7-$level, 
                             'producedResource' => 'archer', 'producedAmount' => 1];
            $pikemanSchema1 = ['construction' => 'forge', 'level' => $level, 'requiredResource' => 'soldier', 'requiredAmount' => 1, 
                             'producedResource' => 'pikeman', 'producedAmount' => 1];
            $pikemanSchema2 = ['construction' => 'forge', 'level' => $level, 'requiredResource' => 'iron', 'requiredAmount' => 7-$level, 
                             'producedResource' => 'pikeman', 'producedAmount' => 1];
            $horsemanSchema1 = ['construction' => 'stable', 'level' => $level, 'requiredResource' => 'soldier', 'requiredAmount' => 1, 
                             'producedResource' => 'horseman', 'producedAmount' => 1];
            $horsemanSchema2 = ['construction' => 'stable', 'level' => $level, 'requiredResource' => 'horse', 'requiredAmount' => 7-$level, 
                             'producedResource' => 'horseman', 'producedAmount' => 1];
            $knightSchema1 = ['construction' => 'militaryAcademy', 'level' => $level, 'requiredResource' => 'soldier', 'requiredAmount' => 1, 
                             'producedResource' => 'knight', 'producedAmount' => 1];
            $knightSchema2 = ['construction' => 'militaryAcademy', 'level' => $level, 'requiredResource' => 'iron', 'requiredAmount' => 7-$level, 
                             'producedResource' => 'knight', 'producedAmount' => 1];
            $knightSchema3 = ['construction' => 'militaryAcademy', 'level' => $level, 'requiredResource' => 'horse', 'requiredAmount' => 7-$level, 
                             'producedResource' => 'knight', 'producedAmount' => 1];
            $mArcherSchema1 = ['construction' => 'militaryAcademy', 'level' => $level, 'requiredResource' => 'soldier', 'requiredAmount' => 1, 
                             'producedResource' => 'mountedArcher', 'producedAmount' => 1];
            $mArcherSchema2 = ['construction' => 'militaryAcademy', 'level' => $level, 'requiredResource' => 'lin', 'requiredAmount' => 7-$level, 
                             'producedResource' => 'mountedArcher', 'producedAmount' => 1];
            $mArcherSchema3 = ['construction' => 'militaryAcademy', 'level' => $level, 'requiredResource' => 'horse', 'requiredAmount' => 7-$level, 
                             'producedResource' => 'mountedArcher', 'producedAmount' => 1];
            $paladinSchema1 = ['construction' => 'guardTower', 'level' => $level, 'requiredResource' => 'soldier', 'requiredAmount' => 1, 
                             'producedResource' => 'paladin', 'producedAmount' => 1];
            $paladinSchema2 = ['construction' => 'guardTower', 'level' => $level, 'requiredResource' => 'iron', 'requiredAmount' => 2*(7-$level), 
                             'producedResource' => 'paladin', 'producedAmount' => 1];
            array_push($this->conversionSchemas, $factorySchema, $butcherSchema, $cauldronSchema, $archerSchema1, $archerSchema2, $pikemanSchema1,
                        $pikemanSchema2, $horsemanSchema1, $horsemanSchema2, $knightSchema1, $knightSchema2, $knightSchema3, $mArcherSchema1, 
                        $mArcherSchema2, $mArcherSchema3, $paladinSchema1, $paladinSchema2);
        }

    }

    public function getDependencies()
    {
        return [ResourceFixtures::class, ConstructionFixtures::class];
    }

    public static function getGroups(): array
    {
        return ['prod'];
    }
}

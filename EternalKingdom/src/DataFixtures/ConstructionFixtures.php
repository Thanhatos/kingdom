<?php

namespace App\DataFixtures;

use App\Entity\Constructions;
use App\Enum\ConstructionType;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Output\ConsoleOutput;

class ConstructionFixtures extends Fixture implements FixtureGroupInterface
{
    public $constructions = [
        //Buildings
        ['name' => 'palace', 'type' => ConstructionType::TYPE_BUILDING],
        ['name' => 'farm', 'type' => ConstructionType::TYPE_BUILDING],
        ['name' => 'attic', 'type' => ConstructionType::TYPE_BUILDING],
        ['name' => 'market', 'type' => ConstructionType::TYPE_BUILDING],
        ['name' => 'hut', 'type' => ConstructionType::TYPE_BUILDING],
        ['name' => 'constructionSite', 'type' => ConstructionType::TYPE_BUILDING],
        ['name' => 'barracks', 'type' => ConstructionType::TYPE_BUILDING],
        ['name' => 'wall', 'type' => ConstructionType::TYPE_BUILDING],
        ['name' => 'headquarters', 'type' => ConstructionType::TYPE_BUILDING],
        ['name' => 'workshop', 'type' => ConstructionType::TYPE_BUILDING],
        ['name' => 'guardTower', 'type' => ConstructionType::TYPE_BUILDING],
        ['name' => 'militaryAcademy', 'type' => ConstructionType::TYPE_BUILDING],
        ['name' => 'archery', 'type' => ConstructionType::TYPE_BUILDING],
        ['name' => 'factory', 'type' => ConstructionType::TYPE_BUILDING],
        ['name' => 'stable', 'type' => ConstructionType::TYPE_BUILDING],
        ['name' => 'butcher', 'type' => ConstructionType::TYPE_BUILDING],
        ['name' => 'forge', 'type' => ConstructionType::TYPE_BUILDING],
        ['name' => 'cauldron', 'type' => ConstructionType::TYPE_BUILDING],
        //Units
        ['name' => 'catapult', 'type' => ConstructionType::TYPE_UNIT],   
        ['name' => 'ballista', 'type' => ConstructionType::TYPE_UNIT]        
    ];

    private $entityManager;
    private $output;

    public function __construct(EntityManagerInterface $em)
    {
        $this->entityManager = $em;
    }

    public function load(ObjectManager $manager)
    {
        $this->output = new ConsoleOutput();
        foreach ($this->constructions as $constructionData)
        {
            $construction = new Constructions();
            $construction->setName($constructionData['name']);
            $construction->setConstructionType($constructionData['type']);
            $manager->persist($construction);
        }
        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['prod'];
    }
}

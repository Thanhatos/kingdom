<?php

namespace App\DataFixtures;

use App\Entity\Lord;
use App\Entity\City;
use App\Entity\BuiltBuildings;
use App\Entity\Storage;
use App\Entity\Resources;
use App\Entity\Constructions;
use App\Enum\TitleType;
use App\Enum\HealthType;
use App\Service\GameDataService;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Output\ConsoleOutput;

class TestFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    private $output;
    private $gameDataService;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->output = new ConsoleOutput();
        $this->gameDataService = new GameDataService($entityManager);
    }


    public function load(ObjectManager $manager)
    {
        $this->gameDataService->loadDicts();

        for($i = 0; $i < 20; $i++)
        {
            $lord = $this->CreateDefaultLord();
            $lord->setName("AlphaTester" . ($i + 1));
            $lord->getCity()->setName('EternalCity' . ($i + 1));
            $manager->persist($lord);
        }

        $emperor = $this->CreateEmperorLord();
        $manager->persist($emperor);

        $manager->flush();
    }

    private function CreateEmperorLord() : Lord
    {
        $emperorLord = $this->createCustomLord('LOUIS XII', 52, 11, TitleType::TYPE_EMPEROR, HealthType::TYPE_GOOD, 95, 95);
        $emperorCity = $this->createDefaultCity();
        $emperorCity->addBuiltBuilding($this->gameDataService->createBuilding('palace', 4));
        $emperorCity->addBuiltBuilding($this->gameDataService->createBuilding('farm', 3));
        $emperorCity->addBuiltBuilding($this->gameDataService->createBuilding('attic', 3));
        $emperorCity->addBuiltBuilding($this->gameDataService->createBuilding('workshop', 1));
        $emperorCity->addBuiltBuilding($this->gameDataService->createBuilding('barracks', 1));
        $emperorCity->addBuiltBuilding($this->gameDataService->createBuilding('cauldron', 3));
        $emperorCity->addBuiltBuilding($this->gameDataService->createBuilding('butcher', 3));
        $emperorCity->addBuiltBuilding($this->gameDataService->createBuilding('factory', 3));
        $emperorCity->addStorage($this->gameDataService->createStorage('worker', 1));
        $emperorCity->addStorage($this->gameDataService->createStorage('soldier', 7));
        $emperorCity->addStorage($this->gameDataService->createStorage('archer', 3));
        $emperorCity->addStorage($this->gameDataService->createStorage('paladin', 10));
        $emperorCity->addStorage($this->gameDataService->createStorage('ballista', 101));
        $emperorCity->addStorage($this->gameDataService->createStorage('horse', 50));
        $emperorCity->addStorage($this->gameDataService->createStorage('lin', 50));
        $emperorCity->addStorage($this->gameDataService->createStorage('wood', 1000));
        $emperorLord->setCity($emperorCity);

        return $emperorLord;
    }

    private function CreateDefaultLord() : Lord
    {
        $lord = new Lord();
        $city  = $this->createDefaultCity();
        $lord ->setCity($city);

        return $lord;
    }

    private function createDefaultCity() : City
    {
        $city = new City();
        $city->setName('EternalKingdom');
        $city->setGrid('');
        $city->setRemainingTurns(100);
        $city->setWastedTurns(0);
        $city->addStorage($this->gameDataService->createStorage('wheat', 30));
        $city->addStorage($this->gameDataService->createStorage('farmer', 1));
        $city->addStorage($this->gameDataService->createStorage('remainingHammers', 1));

        return $city;
    }

    private function createCustomLord(string $name, int $ageYear, int $ageMonth, string $title, string $health, int $glory, int $maxGlory) : Lord
    {
        $lord = new Lord();
        $lord->setName($name);
        $lord->setAgeMonth($ageMonth);
        $lord->setAgeYear($ageYear);
        $lord->setTitle($title);
        $lord->setHealth($health);
        $lord->setGlory($glory);
        $lord->setMaxGlory($maxGlory);

        return $lord;
    }



    public function getDependencies(): array
    {
        return [ResourceFixtures::class, ConstructionFixtures::class, ConstructionRequirementFixtures::class, ConstructionSchemaFixtures::class,
                MaxStorageFixtures::class, ResourceConversionSchemaFixtures::class];
    }

    public static function getGroups(): array
    {
        return ['dev'];
    }
}

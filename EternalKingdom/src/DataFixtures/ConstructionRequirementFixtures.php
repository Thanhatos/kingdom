<?php

namespace App\DataFixtures;

use App\Entity\Constructions;
use App\Entity\Title;
use App\Entity\ConstructionRequirements;
use App\Enum\TitleType;
use App\Service\GameDataService;

use Symfony\Component\Serializer\Encoder\JsonEncoder;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Output\ConsoleOutput;

class ConstructionRequirementFixtures extends Fixture implements FixtureGroupInterface, DependentFixtureInterface
{
    public $constructionsRequirements = [
        ['construction' => 'palace',            'level' => 1,   'requiredConstruction' => null,         'requiredLevel' => null, 'requiredTitle' => null],
        ['construction' => 'farm',              'level' => 1,   'requiredConstruction' => 'palace',     'requiredLevel' => 1,    'requiredTitle' => null],
        ['construction' => 'attic',             'level' => 1,   'requiredConstruction' => 'palace',     'requiredLevel' => 1,    'requiredTitle' => null],
        ['construction' => 'market',            'level' => 1,   'requiredConstruction' => 'palace',     'requiredLevel' => 1,    'requiredTitle' => null],
        ['construction' => 'hut',               'level' => 1,   'requiredConstruction' => 'palace',     'requiredLevel' => 1,    'requiredTitle' => null],
        ['construction' => 'constructionSite', 'level' => 1,   'requiredConstruction' => 'palace',     'requiredLevel' => 1,    'requiredTitle' => null],
        ['construction' => 'barracks',          'level' => 1,   'requiredConstruction' => 'palace',     'requiredLevel' => 1,    'requiredTitle' => null],
        ['construction' => 'wall',              'level' => 1,   'requiredConstruction' => 'barracks',   'requiredLevel' => 1,    'requiredTitle' => null],
        ['construction' => 'headquarters',      'level' => 1,   'requiredConstruction' => 'barracks',   'requiredLevel' => 1,    'requiredTitle' => null],
        ['construction' => 'workshop',          'level' => 1,   'requiredConstruction' => 'barracks',   'requiredLevel' => 1,    'requiredTitle' => null],
        ['construction' => 'guardTower',       'level' => 1,   'requiredConstruction' => 'barracks',   'requiredLevel' => 1,    'requiredTitle' => null],
        ['construction' => 'militaryAcademy',  'level' => 1,   'requiredConstruction' => 'barracks',   'requiredLevel' => 1,    'requiredTitle' => null],
        ['construction' => 'archery',           'level' => 1,   'requiredConstruction' => 'palace',     'requiredLevel' => 1,    'requiredTitle' => null],
        ['construction' => 'factory',           'level' => 1,   'requiredConstruction' => 'archery',    'requiredLevel' => 1,    'requiredTitle' => null],
        ['construction' => 'stable',            'level' => 1,   'requiredConstruction' => 'palace',     'requiredLevel' => 1,    'requiredTitle' => null],
        ['construction' => 'butcher',           'level' => 1,   'requiredConstruction' => 'stable',     'requiredLevel' => 1,    'requiredTitle' => null],
        ['construction' => 'forge',             'level' => 1,   'requiredConstruction' => 'palace',     'requiredLevel' => 1,    'requiredTitle' => null],
        ['construction' => 'cauldron',          'level' => 1,   'requiredConstruction' => 'forge',      'requiredLevel' => 1,    'requiredTitle' => null],
        ['construction' => 'catapult',          'level' => 1,   'requiredConstruction' => 'workshop',   'requiredLevel' => 1,    'requiredTitle' => null],
        ['construction' => 'ballista',          'level' => 1,   'requiredConstruction' => 'workshop',   'requiredLevel' => 1,    'requiredTitle' => null],
    ];

    private $manager;
    private $output;
    private $gameDataService;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->output = new ConsoleOutput();
        $this->gameDataService = new GameDataService($entityManager);
    }

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $this->gameDataService->loadDicts();

        $this->fillData();

        foreach ($this->constructionsRequirements as $RequirementsData)
        {
            [$constructionDict, $titleDict] = [$this->gameDataService->getConstructionDict(), $this->gameDataService->getTitleDict()];
            $constructionRequirements = new ConstructionRequirements();
            
            $constructionRequirements->setConstruction($constructionDict[$RequirementsData['construction']]);
            $constructionRequirements->setLevel($RequirementsData['level']);
            if($RequirementsData['requiredConstruction'] != null)
            {
                $constructionRequirements->setRequiredConstruction($constructionDict[$RequirementsData['requiredConstruction']]);
                $constructionRequirements->setRequiredLevel($RequirementsData['requiredLevel']);
            }
            if ($RequirementsData['requiredTitle'] != null)
            {
                $constructionRequirements->setRequiredTitle($titleDict[$RequirementsData['requiredTitle']]);
            }
            $manager->persist($constructionRequirements);
        }
        $manager->flush();
    }

    //Determines the superior levels requirements from level1 requirement
    private function fillData()
    {
        $title = TitleType::getAvailableTypes();
        foreach($this->constructionsRequirements as $requirement)
        {
            if($requirement['construction'] == 'palace')
            {
                for($level = 2; $level < 6; $level++)
                {
                    $requirement = ['construction' => 'palace', 'level' => $level, 'requiredConstruction' => null, 'requiredLevel' => null, 'requiredTitle' => $title[2*$level - 3]];
                    array_push($this->constructionsRequirements, $requirement);
                } 
            } else
            {
                for($level = 2; $level < 6; $level++)
                {
                    $requirement = ['construction' => $requirement['construction'], 'level' => $level, 'requiredConstruction' => $requirement['requiredConstruction'], 'requiredLevel' => $level, 'requiredTitle' => null];
                    array_push($this->constructionsRequirements, $requirement);
                } 
            }    
        }
    }

    public function getDependencies()
    {
        return [TitleFixtures::class, ConstructionFixtures::class];
    }

    public static function getGroups(): array
    {
        return ['prod'];
    }
}

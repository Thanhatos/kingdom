<?php

namespace App\DataFixtures;

use App\Entity\Constructions;
use App\Entity\Resources;
use App\Entity\ConstructionSchemas;
use App\Enum\ConstructionType;
use App\Service\GameDataService;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Output\ConsoleOutput;

class ConstructionSchemaFixtures extends Fixture implements FixtureGroupInterface, DependentFixtureInterface
{
    public $constructionsSchemas = [
        ['construction' => 'palace',            'level' => 1,     'resource' => 'wheat',            'amount' => 40],
        ['construction' => 'palace',            'level' => 1,     'resource' => 'gold',             'amount' => 100],
        ['construction' => 'palace',            'level' => 1,     'resource' => 'wood',             'amount' => 100],
        ['construction' => 'palace',            'level' => 1,     'resource' => 'remainingHammers', 'amount' => 12],
        ['construction' => 'farm',              'level' => 1,     'resource' => 'wood',             'amount' => 20],
        ['construction' => 'farm',              'level' => 1,     'resource' => 'remainingHammers', 'amount' => 8],
        ['construction' => 'attic',             'level' => 1,     'resource' => 'wood',             'amount' => 40],
        ['construction' => 'attic',             'level' => 1,     'resource' => 'remainingHammers', 'amount' => 4],
        ['construction' => 'market',            'level' => 1,     'resource' => 'gold',             'amount' => 20],
        ['construction' => 'market',            'level' => 1,     'resource' => 'wood',             'amount' => 40],
        ['construction' => 'market',            'level' => 1,     'resource' => 'remainingHammers', 'amount' => 4],
        ['construction' => 'hut',               'level' => 1,     'resource' => 'wood',             'amount' => 60],
        ['construction' => 'hut',               'level' => 1,     'resource' => 'remainingHammers', 'amount' => 4],
        ['construction' => 'constructionSite',  'level' => 1,     'resource' => 'wheat',            'amount' => 40],
        ['construction' => 'constructionSite',  'level' => 1,     'resource' => 'wood',             'amount' => 40],
        ['construction' => 'constructionSite',  'level' => 1,     'resource' => 'remainingHammers', 'amount' => 8],
        ['construction' => 'barracks',          'level' => 1,     'resource' => 'wheat',            'amount' => 40],
        ['construction' => 'barracks',          'level' => 1,     'resource' => 'gold',             'amount' => 100],
        ['construction' => 'barracks',          'level' => 1,     'resource' => 'remainingHammers', 'amount' => 2],
        ['construction' => 'wall',              'level' => 1,     'resource' => 'wood',             'amount' => 200],
        ['construction' => 'wall',              'level' => 1,     'resource' => 'remainingHammers', 'amount' => 10],
        ['construction' => 'headquarters',      'level' => 1,     'resource' => 'wheat',            'amount' => 40],
        ['construction' => 'headquarters',      'level' => 1,     'resource' => 'gold',             'amount' => 100],
        ['construction' => 'headquarters',      'level' => 1,     'resource' => 'remainingHammers', 'amount' => 4],
        ['construction' => 'workshop',          'level' => 1,     'resource' => 'gold',             'amount' => 40],
        ['construction' => 'workshop',          'level' => 1,     'resource' => 'wood',             'amount' => 40],
        ['construction' => 'workshop',          'level' => 1,     'resource' => 'remainingHammers', 'amount' => 16],
        ['construction' => 'guardTower',        'level' => 1,     'resource' => 'wheat',            'amount' => 40],
        ['construction' => 'guardTower',        'level' => 1,     'resource' => 'lin',              'amount' => 5],
        ['construction' => 'guardTower',        'level' => 1,     'resource' => 'iron',             'amount' => 5],
        ['construction' => 'guardTower',        'level' => 1,     'resource' => 'remainingHammers', 'amount' => 22],
        ['construction' => 'militaryAcademy',   'level' => 1,     'resource' => 'wheat',            'amount' => 40],
        ['construction' => 'militaryAcademy',   'level' => 1,     'resource' => 'gold',             'amount' => 160],
        ['construction' => 'militaryAcademy',   'level' => 1,     'resource' => 'remainingHammers', 'amount' => 6],
        ['construction' => 'archery',           'level' => 1,     'resource' => 'gold',             'amount' => 60],
        ['construction' => 'archery',           'level' => 1,     'resource' => 'wood',             'amount' => 160],
        ['construction' => 'archery',           'level' => 1,     'resource' => 'lin',              'amount' => 5],
        ['construction' => 'archery',           'level' => 1,     'resource' => 'remainingHammers', 'amount' => 2],
        ['construction' => 'factory',           'level' => 1,     'resource' => 'gold',             'amount' => 60],
        ['construction' => 'factory',           'level' => 1,     'resource' => 'wood',             'amount' => 100],
        ['construction' => 'factory',           'level' => 1,     'resource' => 'remainingHammers', 'amount' => 4],
        ['construction' => 'stable',            'level' => 1,     'resource' => 'gold',             'amount' => 140],
        ['construction' => 'stable',            'level' => 1,     'resource' => 'wood',             'amount' => 60],
        ['construction' => 'stable',            'level' => 1,     'resource' => 'horse',            'amount' => 5],
        ['construction' => 'stable',            'level' => 1,     'resource' => 'remainingHammers', 'amount' => 4],
        ['construction' => 'butcher',           'level' => 1,     'resource' => 'wheat',            'amount' => 30],
        ['construction' => 'butcher',           'level' => 1,     'resource' => 'gold',             'amount' => 120],
        ['construction' => 'butcher',           'level' => 1,     'resource' => 'remainingHammers', 'amount' => 2],
        ['construction' => 'forge',             'level' => 1,     'resource' => 'wheat',            'amount' => 20],
        ['construction' => 'forge',             'level' => 1,     'resource' => 'wood',             'amount' => 60],
        ['construction' => 'forge',             'level' => 1,     'resource' => 'iron',             'amount' => 5],
        ['construction' => 'forge',             'level' => 1,     'resource' => 'remainingHammers', 'amount' => 14],
        ['construction' => 'cauldron',          'level' => 1,     'resource' => 'wood',             'amount' => 80],
        ['construction' => 'cauldron',          'level' => 1,     'resource' => 'remainingHammers', 'amount' => 12],
        ['construction' => 'catapult',          'level' => 1,     'resource' => 'wood',             'amount' => 60],
        ['construction' => 'catapult',          'level' => 1,     'resource' => 'remainingHammers', 'amount' => 24],
        ['construction' => 'ballista',          'level' => 1,     'resource' => 'wood',             'amount' => 240],
        ['construction' => 'ballista',          'level' => 1,     'resource' => 'remainingHammers', 'amount' => 6]
        
    ];

    private $manager;
    private $output;
    private $gameDataService;
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->output = new ConsoleOutput();
        $this->gameDataService = new GameDataService($this->entityManager);
    }

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;   
        $this->gameDataService->loadDicts();

        $this->fillData();

        foreach ($this->constructionsSchemas as $schemaData)
        {
            [$constructionDict, $resourceDict] = [$this->gameDataService->getConstructionDict(), $this->gameDataService->getResourceDict()];
            $schema = new ConstructionSchemas();
            $schema->setConstruction($constructionDict[$schemaData['construction']]);
            $schema->setLevel($schemaData['level']);
            $schema->setResource($resourceDict[$schemaData['resource']]);
            $schema->setAmount($schemaData['amount']);
            $manager->persist($schema);
        }
        $manager->flush();
    }

    //Determines superior level costs from level1 cost
    private function fillData()
    {
        foreach($this->constructionsSchemas as $schema)
        {
            if ($schema['construction'] == 'catapult' || $schema['construction'] == 'ballista')
            {
                switch($schema['resource'])
                {
                    case 'wood':
                        $schemaLevel2 = ['construction' => $schema['construction'], 'level' => 2, 'resource' => 'wood', 'amount' => $schema['amount']*0.8];
                        $schemaLevel3 = ['construction' => $schema['construction'], 'level' => 3, 'resource' => 'wood', 'amount' => $schema['amount']*0.7];
                        $schemaLevel4 = ['construction' => $schema['construction'], 'level' => 4, 'resource' => 'wood', 'amount' => $schema['amount']*0.6];
                        $schemaLevel5 = ['construction' => $schema['construction'], 'level' => 5, 'resource' => 'wood', 'amount' => $schema['amount']*0.5];
                        array_push($this->constructionsSchemas, $schemaLevel2, $schemaLevel3, $schemaLevel4, $schemaLevel5);
                        break;
                    case 'remainingHammers':
                        $schemaLevel2 = ['construction' => $schema['construction'], 'level' => 2, 'resource' => 'remainingHammers', 'amount' => $schema['amount']];
                        $schemaLevel3 = ['construction' => $schema['construction'], 'level' => 3, 'resource' => 'remainingHammers', 'amount' => $schema['amount']];
                        $schemaLevel4 = ['construction' => $schema['construction'], 'level' => 4, 'resource' => 'remainingHammers', 'amount' => $schema['amount']];
                        $schemaLevel5 = ['construction' => $schema['construction'], 'level' => 5, 'resource' => 'remainingHammers', 'amount' => $schema['amount']];
                        array_push($this->constructionsSchemas, $schemaLevel2, $schemaLevel3, $schemaLevel4, $schemaLevel5);
                }     
            } else
            {
                switch($schema['resource'])
                {
                    case 'wheat':
                        $schemaLevel2 = ['construction' => $schema['construction'], 'level' => 2, 'resource' => 'wheat', 'amount' => $schema['amount']*2.5];
                        $schemaLevel3 = ['construction' => $schema['construction'], 'level' => 3, 'resource' => 'wheat', 'amount' => $schema['amount']*5];
                        $schemaLevel4 = ['construction' => $schema['construction'], 'level' => 4, 'resource' => 'wheat', 'amount' => $schema['amount']*20];
                        $schemaLevel5 = ['construction' => $schema['construction'], 'level' => 5, 'resource' => 'wheat', 'amount' => $schema['amount']*50];
                        array_push($this->constructionsSchemas, $schemaLevel2, $schemaLevel3, $schemaLevel4, $schemaLevel5);
                        break;
                    case 'wood':
                        $schemaLevel2 = ['construction' => $schema['construction'], 'level' => 2, 'resource' => 'wood', 'amount' => $schema['amount']*2.5];
                        $schemaLevel3 = ['construction' => $schema['construction'], 'level' => 3, 'resource' => 'wood', 'amount' => $schema['amount']*5];
                        $schemaLevel4 = ['construction' => $schema['construction'], 'level' => 4, 'resource' => 'wood', 'amount' => $schema['amount']*20];
                        $schemaLevel5 = ['construction' => $schema['construction'], 'level' => 5, 'resource' => 'wood', 'amount' => $schema['amount']*47.5];
                        array_push($this->constructionsSchemas, $schemaLevel2, $schemaLevel3, $schemaLevel4, $schemaLevel5);
                        break;
                    case 'gold':
                        $schemaLevel2 = ['construction' => $schema['construction'], 'level' => 2, 'resource' => 'gold', 'amount' => $schema['amount']*2];
                        $schemaLevel3 = ['construction' => $schema['construction'], 'level' => 3, 'resource' => 'gold', 'amount' => $schema['amount']*4];
                        $schemaLevel4 = ['construction' => $schema['construction'], 'level' => 4, 'resource' => 'gold', 'amount' => $schema['amount']*15];
                        $schemaLevel5 = ['construction' => $schema['construction'], 'level' => 5, 'resource' => 'gold', 'amount' => $schema['amount']*32.5];
                        array_push($this->constructionsSchemas, $schemaLevel2, $schemaLevel3, $schemaLevel4, $schemaLevel5);
                        break;
                    case 'remainingHammers':
                        $schemaLevel2 = ['construction' => $schema['construction'], 'level' => 2, 'resource' => 'remainingHammers', 'amount' => $schema['amount']*2];
                        $schemaLevel3 = ['construction' => $schema['construction'], 'level' => 3, 'resource' => 'remainingHammers', 'amount' => $schema['amount']*4.5];
                        $schemaLevel4 = ['construction' => $schema['construction'], 'level' => 4, 'resource' => 'remainingHammers', 'amount' => $schema['amount']*10];
                        $schemaLevel5 = ['construction' => $schema['construction'], 'level' => 5, 'resource' => 'remainingHammers', 'amount' => $schema['amount']*20];
                        array_push($this->constructionsSchemas, $schemaLevel2, $schemaLevel3, $schemaLevel4, $schemaLevel5);
                        break;
                    default: //lin, iron, horse
                        $schemaLevel2 = ['construction' => $schema['construction'], 'level' => 2, 'resource' => 'wheat', 'amount' => $schema['amount']*4];
                        $schemaLevel3 = ['construction' => $schema['construction'], 'level' => 3, 'resource' => 'wheat', 'amount' => $schema['amount']*8];
                        $schemaLevel4 = ['construction' => $schema['construction'], 'level' => 4, 'resource' => 'wheat', 'amount' => $schema['amount']*14];
                        $schemaLevel5 = ['construction' => $schema['construction'], 'level' => 5, 'resource' => 'wheat', 'amount' => $schema['amount']*20];
                        array_push($this->constructionsSchemas, $schemaLevel2, $schemaLevel3, $schemaLevel4, $schemaLevel5);
                        break;
                }
            }    
        }
        array_splice($this->constructionsSchemas, 0, 4);
        $palaceWoodSchema = ['construction' => 'palace', 'level' => 1, 'resource' => 'wood', 'amount' => 15];
        $palaceHammerSchema = ['construction' => 'palace', 'level' => 1, 'resource' => 'remainingHammers', 'amount' => 2];
        array_unshift($this->constructionsSchemas, $palaceHammerSchema, $palaceWoodSchema);
        
    }

    public function getDependencies()
    {
        return [ResourceFixtures::class, ConstructionFixtures::class];
    }

    public static function getGroups(): array
    {
        return ['prod'];
    }
}

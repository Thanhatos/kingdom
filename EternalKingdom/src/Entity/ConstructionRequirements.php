<?php

namespace App\Entity;

use App\Repository\ConstructionRequirementsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ConstructionRequirementsRepository::class)
 */
class ConstructionRequirements
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Constructions::class, inversedBy="constructionRequirements")
     * @ORM\JoinColumn(nullable=false)
     */
    private $construction;

    /**
     * @ORM\Column(type="integer")
     */
    private $level;

    /**
     * @ORM\ManyToOne(targetEntity=Constructions::class)
     * @ORM\JoinColumn(nullable=true)
     */
    private $requiredConstruction;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $requiredLevel;

    /**
     * @ORM\ManyToOne(targetEntity=Titles::class)
     */
    private $requiredTitle;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getConstruction(): ?Constructions
    {
        return $this->construction;
    }

    public function setConstruction(?Constructions $construction): self
    {
        $this->construction = $construction;

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getRequiredConstruction(): ?Constructions
    {
        return $this->requiredConstruction;
    }

    public function setRequiredConstruction(?Constructions $requiredConstruction): self
    {
        $this->requiredConstruction = $requiredConstruction;

        return $this;
    }

    public function getRequiredLevel(): ?int
    {
        return $this->requiredLevel;
    }

    public function setRequiredLevel(?int $requiredLevel): self
    {
        $this->requiredLevel = $requiredLevel;

        return $this;
    }

    public function getRequiredTitle(): ?Titles
    {
        return $this->requiredTitle;
    }

    public function setRequiredTitle(?Titles $requiredTitle): self
    {
        $this->requiredTitle = $requiredTitle;

        return $this;
    }
}

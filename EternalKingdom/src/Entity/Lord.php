<?php

namespace App\Entity;

use App\Enum\TitleType;
use App\Enum\HealthType;
use App\Repository\LordRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Lord is a character that is playing a game.
 * If a player has no lord, he doesn't play any game.
 *
 * @ORM\Entity(repositoryClass=LordRepository::class)
 */
class Lord
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $ageYear = 20;

    /**
     * @ORM\Column(type="float")
     */
    private $ageMonth = 0;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $health = HealthType::TYPE_EXCELLENT;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $title = TitleType::TYPE_KNIGHT;

    /**
     * @ORM\Column(type="integer")
     */
    private $glory = 1;

    /**
     * @ORM\Column(type="integer")
     */
    private $maxGlory = 1;

    /**
     * @ORM\OneToOne(targetEntity=City::class, mappedBy="lord", cascade={"persist", "remove"})
     */
    private $city;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAgeYear(): ?int
    {
        return $this->ageYear;
    }

    public function setAgeYear(int $ageYear): self
    {
        $this->ageYear = $ageYear;

        return $this;
    }

    public function getAgeMonth(): ?float
    {
        return $this->ageMonth;
    }

    public function setAgeMonth(float $ageMonth): self
    {
        $this->ageMonth = $ageMonth;

        return $this;
    }

    public function getHealth(): ?string
    {
        return $this->health;
    }

    public function setHealth(string $health): self
    {
        if(!in_array($health, HealthType::getAvailableTypes()))
        {
            throw new InvalidArgumentException("Invalid type");
        }
        $this->health = $health;
        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        if(!in_array($title, TitleType::getAvailableTypes()))
        {
            throw new InvalidArgumentException("Invalid type");
        }
        $this->title = $title;
        return $this;
    }

    public function getGlory(): ?int
    {
        return $this->glory;
    }

    public function setGlory(int $glory): self
    {
        $this->glory = $glory;

        return $this;
    }

    public function getMaxGlory(): ?int
    {
        return $this->maxGlory;
    }

    public function setMaxGlory(int $maxGlory): self
    {
        $this->maxGlory = $maxGlory;

        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function &getCityRef() : ?City
    {
        return $this->city;
    }

    public function setCity(City $city): self
    {
        // set the owning side of the relation if necessary
        if ($city->getLord() !== $this) {
            $city->setLord($this);
        }

        $this->city = $city;

        return $this;
    }

}

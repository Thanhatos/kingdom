<?php

namespace App\Enum;

/**
 * Enum used to represent the lord wealth
 *
 * Class WealthEnum
 * @package App\Enum
 */
class HealthType
{
    const TYPE_EXCELLENT    = "excellent";
    const TYPE_GOOD         = "good";
    const TYPE_BAD          = "bad";
    const TYPE_DEAD         = "dead";

    /** @var array user friendly named type */
    protected static $typeName = [
        self::TYPE_EXCELLENT    => 'excellent',
        self::TYPE_GOOD         => 'good',
        self::TYPE_BAD          => 'bad',
        self::TYPE_DEAD         => 'dead',
    ];

    /**
     * @param string $typeShortName
     * @return string
     */
    public static function getTypeName(string $typeShortName) : string
    {
        if (!isset(static::$typeName[$typeShortName])) {
            return "Unknown type ($typeShortName)";
        }

        return static::$typeName[$typeShortName];
    }

    /**
     * @return array<string>
     */
    public static function getAvailableTypes(): array
    {
        return [
            self::TYPE_EXCELLENT,
            self::TYPE_GOOD,
            self::TYPE_BAD,
            self::TYPE_DEAD
        ];
    }
}
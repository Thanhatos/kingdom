<?php

namespace App\Enum;

/**
 * Enum used to represent the lord wealth
 *
 * Class WealthEnum
 * @package App\Enum
 */
class TitleType
{
    const TYPE_KNIGHT       = "knight";     // 1
    const TYPE_LORD         = "lord";       // 3
    const TYPE_BARON        = "baron";      // 7
    const TYPE_VISCOUNT     = "viscount";   // 10
    const TYPE_COUNT        = "count";      // 15
    const TYPE_MARQUIS      = "marquis";    // 20
    const TYPE_DUKE         = "duke";       // 30
    const TYPE_PRINCE       = "prince";     // 50
    const TYPE_KING         = "king";       // 75
    const TYPE_EMPEROR      = "emperor";    // 100


    /**
     * @return array<string>
     */
    public static function getAvailableTypes(): array
    {
        return [
            self::TYPE_KNIGHT,
            self::TYPE_LORD,
            self::TYPE_BARON,
            self::TYPE_VISCOUNT,
            self::TYPE_COUNT,
            self::TYPE_MARQUIS,
            self::TYPE_DUKE,
            self::TYPE_PRINCE,
            self::TYPE_KING,
            self::TYPE_EMPEROR
        ];
    }
}
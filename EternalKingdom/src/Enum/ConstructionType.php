<?php

namespace App\Enum;

/**
 * Enum used to represent the construction types
 *
 * Class ConstrucionEnum
 * @package App\Enum
 */
class ConstructionType
{
    const TYPE_BUILDING  = "building";     
    const TYPE_UNIT      = "unit";       


    /**
     * @return array<string>
     */
    public static function getAvailableTypes(): array
    {
        return [
            self::TYPE_BUILDING,
            self::TYPE_UNIT
        ];
    }
}
<?php

namespace App\Enum;

/**
 * Enum used to represent the resource types
 *
 * Class ResourceEnum
 * @package App\Enum
 */
class ResourceType
{
    const TYPE_RESOURCE    = "resource";     
    const TYPE_POPULATION  = "population";       
    const TYPE_ARMY        = "army";     

    /**
     * @return array<string>
     */
    public static function getAvailableTypes(): array
    {
        return [
            self::TYPE_RESOURCE,
            self::TYPE_POPULATION,
            self::TYPE_ARMY    
        ];
    }
}
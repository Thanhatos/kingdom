<?php

namespace App\Service;

use App\Enum\ResourceType;
use App\Model\Alignment;
use App\Entity\Storage;
use App\Entity\BuiltBuildings;
use App\Entity\Lord;
use App\Entity\City;
use App\Entity\Resources;
use App\Entity\Constructions;
use App\Entity\ConstructionRequirements;
use App\Entity\ConstructionSchemas;
use App\Entity\MaxStorage;
use App\Service\GameDataService;

/**
 * City manager aims to :
 * - Reacts when alignments are made
 * - Manage Resources, Population, Armies and buildings
 */
class CityManager
{
    const MAX_TURN_COUNT = 100;

    // Building Production Bonus per level (0 to 5)
    const BUILDING_LEVEL_MULTIPLIER = [1, 1.3, 1.55, 1.75, 1.9, 2];
    // Alignment length bonus per size (3 to 8)
    const ALIGNMENT_LENGTH_MULTIPLIER = [1, 1.25, 1.5, 2, 3, 4];

    private $entityManager;

    //Buffers for all calculations
    private $steps;
    private $city;
    private $cumulativeGains;

    public $gameDataService;

    /**
     * CityController constructor.
     * @param GameDataService $gameDataService
     */
    public function __construct(GameDataService $gameDataService)
    {
        $this->gameDataService = $gameDataService;
        $this->gameDataService->loadDicts();
    }

    // Increments the specified villager type and removes one citizen
    public function convertCitizen(City $city, int $villagerType) : bool
    {
        $this->city = $city;
        if($this->city->getStorageCount('citizen') <= 0)
        {
            return false;
        }

        $villagerName = "";
        switch ($villagerType)
        {
            case 0: $villagerName = 'farmer';       break;
            case 1: $villagerName = 'lumberjack';   break;
            case 2: $villagerName = 'worker';       break;
            case 3: $villagerName = 'merchant';     break;
            case 4: $villagerName = 'recruiter';    break;
            default: break;
        }

        if(!empty($villagerName))
        {
            $storage = $this->getStorage($villagerName);
            $storage->setAmount($storage->getAmount() + 1);
            $storage = $this->getStorage('citizen');
            $storage->setAmount($storage->getAmount() - 1);

            return true;
        }

        return false;
    }

    //Triggered once the turn is over, update resources, population, buildings, army...
    public function convertAlignments(City $city, array $steps)
    {
        $this->city = $city;
        $this->steps = $steps;
        $this->city->setRemainingTurns($this->city->getRemainingTurns() - 1);
        $this->convertAlignmentsToResources();

        if ($this->cumulativeGains['replay'] == 0)
        {
            $this->consumeFood();
            $this->consumeGold();
            $this->makeOld();
        }

        $this->collectResources();
        if ($this->getStorage('wheat')->getAmount() < 0)
        {
            $this->triggerStarvation();
        }
        if($this->getStorage('gold')->getAmount() < 0)
        {
            $this->triggerBankrupt();
        }
    }

    //Add alignment production to $cumulativeGains variable
    private function convertAlignmentsToResources()
    {
        $this->cumulativeGains = ['wheat' => 0, 'gold' => 0, 'wood' => 0, 'wheatFromHammer' => 0, 'goldFromHammer' => 0,
                                  'hammer' => 0, 'soldier' => 0, 'citizen' => 0, 'replay' => 0];

        for ($step = 0; $step < count($this->steps); $step++)
        {
            // Skip any step that doesn't have alignments (f.i. RESET) !
            if(!isset($this->steps[$step]->alignments))
            {
                continue;
            }

            for ($i = 0; $i < count($this->steps[$step]->alignments); $i++)
            {
                $alignment = $this->steps[$step]->alignments[$i]; // just to read value not modify it!
                $gain = 0;
                switch($alignment->element)
                {
                    case 0:
                        $gain = $this->getWheatProduction($alignment);
                        $this->cumulativeGains['wheat'] += $gain;
                        break;
                    case 1:
                        $gain = $this->getGoldProduction($alignment);
                        $this->cumulativeGains['gold'] += $gain;
                        break;
                    case 2:
                        $gain = $this->GetWoodProduction($alignment);
                        $this->cumulativeGains['wood'] += $gain;
                        break;
                    case 3:
                        if ($this->city->getConstructionProject() !== null)
                        {
                            $gain = $this->getHammerProduction($alignment);
                            $this->cumulativeGains['hammer'] += $gain;
                        } else if ( $this->city->getBuildingLevel('constructionSite') > 0)
                        {
                            $gain = $this->getConstructionSiteProduction($alignment);
                            $this->cumulativeGains['wheatFromHammer'] += $gain;
                            $this->cumulativeGains['goldFromHammer'] += $gain;
                        }
                        break;
                    case 4:
                        $gain = $this->getSoldierProduction($alignment);
                        $this->cumulativeGains['soldier'] += $gain;
                        break;
                    case 5:
                        $gain = 1;
                        $this->cumulativeGains['citizen']++;
                        break;
                    case 6:
                        $gain = $this->getReplayProduction($alignment);
                        $this->cumulativeGains['replay'] += $gain;
                        break;
                }

                $this->steps[$step]->alignments[$i]->gain = $gain;
            }
        }
    }


    /* Production Forumlas */

    private function getWheatProduction(Alignment $alignment) : int
    {
        $jobMultiplier = $this->city->getStorageCount('farmer');
        $buildingMultiplier = CityManager::BUILDING_LEVEL_MULTIPLIER[$this->city->getBuildingLevel('farm')];
        $alignmentMultiplier = CityManager::ALIGNMENT_LENGTH_MULTIPLIER[$alignment->getLength()-3];

        return floor(6 * $buildingMultiplier * $alignmentMultiplier * $jobMultiplier);
    }

    private function getGoldProduction(Alignment $alignment) : int
    {
        $jobMultiplier = $this->city->getStorageCount('merchant');
        $buildingMultiplier = CityManager::BUILDING_LEVEL_MULTIPLIER[$this->city->getBuildingLevel('market')];
        $alignmentMultiplier = CityManager::ALIGNMENT_LENGTH_MULTIPLIER[$alignment->getLength()-3];

        return floor(max(1, 6 * $buildingMultiplier * $alignmentMultiplier * $jobMultiplier));
    }

    private function getWoodProduction(Alignment $alignment) : int
    {
        $jobMultiplier = $this->city->getStorageCount('lumberjack');
        $buildingMultiplier = CityManager::BUILDING_LEVEL_MULTIPLIER[$this->city->getBuildingLevel('hut')];
        $alignmentMultiplier = CityManager::ALIGNMENT_LENGTH_MULTIPLIER[$alignment->getLength()-3];
        return floor(max(1, 5 * $buildingMultiplier * $alignmentMultiplier * $jobMultiplier));
    }

    private function getHammerProduction(Alignment $alignment) : int
    {
        $jobMultiplier = $this->city->getStorageCount('worker');
        $buildingMultiplier = CityManager::BUILDING_LEVEL_MULTIPLIER[$this->city->getBuildingLevel('workshop')];
        $alignmentMultiplier = CityManager::ALIGNMENT_LENGTH_MULTIPLIER[$alignment->getLength()-3];
        return floor($buildingMultiplier * $alignmentMultiplier * $jobMultiplier);
    }

    private function getConstructionSiteProduction(Alignment $alignment) : int
    {
        $alignmentMultiplier = CityManager::ALIGNMENT_LENGTH_MULTIPLIER[$alignment->getLength()-3];
        $constructionSiteLevel = $this->city->getBuildingLevel('constructionSite');
        $buildingMultiplier = CityManager::BUILDING_LEVEL_MULTIPLIER[$this->city->getBuildingLevel('workshop')];
        $workerCount = $this->city->getStorageCount('worker');
        return floor($alignmentMultiplier * $constructionSiteLevel * $buildingMultiplier * $workerCount);
    }

    private function getSoldierProduction(Alignment $alignment) : int
    {
        if ($this->city->getBuildingLevel('barracks') == 0 || !$this->city->isRecruiting())
        {
            return 0;
        }
        $jobBonus = $this->city->getStorageCount('recruiter');
        return floor($jobBonus + $alignment->getLength() - 2);
    }

    private function getReplayProduction(Alignment $alignment) : int
    {
        return ($alignment->getLength() - 2);
    }


    //Add $cumulativeGains variable to stock resources
    private function collectResources()
    {
        $maxStorages = $this->getMaxStorages($this->city);
        foreach ($this->cumulativeGains as $resource => $gains)
        {
            // TODO Factorize & do a PostResourceChange with the switch
            switch($resource)
            {
                case 'wheat':
                case 'wheatFromHammer':
                    $storage = $this->getStorage('wheat');
                    $storage->setAmount(min($maxStorages['wheat'], $storage->getAmount() + $gains));
                    break;
                case 'gold':
                case 'goldFromHammer':
                    $storage = $this->getStorage('gold');
                    $storage->setAmount(min($maxStorages['gold'], $storage->getAmount() + $gains));
                    break;
                case 'wood':
                    $storage = $this->getStorage('wood');
                    $storage->setAmount(min($maxStorages['wood'], $storage->getAmount() + $gains));
                    break;
                case 'hammer':
                    $storage = $this->getStorage('remainingHammers');
                    $storage->setAmount($storage->getAmount() - $gains);
                    if ($storage->getAmount() <= 0 && $this->city->getConstructionProject() != null)
                    {
                        $this->finishConstruction();
                    }
                    break;
                case 'soldier':
                    $storage = $this->getStorage('soldier');
                    $storage->setAmount($storage->getAmount() + $gains);
                    break;
                case 'citizen':
                    $storage = $this->getStorage('citizen');
                    $storage->setAmount($storage->getAmount() + $gains);
                    break;
                case 'replay':
                    $this->city->setRemainingTurns(min(100,$this->city->getRemainingTurns() + $gains));
                    break;
            }
        }
    }

    //Remove wheat consumption from wheat storage
    private function consumeFood()
    {
        $storage = $this->getStorage('wheat');
        $storage->setAmount($storage->getAmount() - $this->city->getPopulationCount());
    }

    //Remove gold consumption from gold storage
    private function consumeGold()
    {
        //TODO detect whole commerce/army
    }

    //Grow the lord old by 1 month every 4 turns
    private function makeOld()
    {
        $lord = $this->city->getLord();
        $lord->setAgeMonth($lord->getAgeMonth() + 0.25);
        if ($lord->getAgeMonth() >= 12)
        {
            $lord->setAgeYear($lord->getAgeYear() + 1);
            $lord->setAgeMonth(0);
        }
    }

    //Kill one random villager, except the last farmer
    private function triggerStarvation()
    {
        $aliveCitizenTypes = [];
        foreach ($this->city->getStorages() as $storage)
        {
            if ($storage->getResource()->getResourceType() === ResourceType::TYPE_POPULATION && $storage->getAmount() > 0)
            {
                // Ignore last farmer!
                if ($storage->getAmount() === 1 && $storage->getResource()->getName() === "farmer")
                    continue;

                array_push($aliveCitizenTypes, $storage->getResource()->getName());
            }
        }

        if (count($aliveCitizenTypes) === 0)
        {
            return;
        }

        $starvationRand = 0;
        if (count($aliveCitizenTypes) > 1)
        {
            $starvationRand = random_int(0, count($aliveCitizenTypes)-1);
        }

        $storage = $this->getStorage($aliveCitizenTypes[$starvationRand]);
        $storage->setAmount($storage->getAmount() - 1);

        $storage = $this->getStorage('wheat');
        $storage->setAmount($this->city->getPopulationCount());
    }

    private function triggerBankrupt()
    {
        //Todo decrement army, give money
    }

    //Set the construction project to null, add the buildBuilding/Unit to DB
    private function finishConstruction()
    {
        $construction = $this->city->getConstructionProject();
        if($construction == null)
            return;

        if ($construction->getConstructionType() == 'building')
        {
            if ($this->city->getBuildingLevel($construction->getName()) > 0 )
            {
                $building = $this->getBuiltBuilding($construction->getName());
                $building->setLevel($this->city->getProjectLevel());
            }
            else
            {
                $building = new BuiltBuildings();
                $building->setBuilding($construction);
                $building->setLevel($this->city->getProjectLevel());
                $this->city->addBuiltBuilding($building);
            }

            $this->city->setConstructionProject(null);
            $this->city->setProjectLevel(null);
        } else if ($construction->getConstructionType() == 'unit')
        {
            $storage = $this->getStorage($construction->getName());
            $storage->setAmount($storage->getAmount()+1);
            $this->city->setConstructionProject(null);
            $this->city->setProjectLevel(null);
        }
    }

    //Return an array of ['construction' => $construction, 'level' => $level, 'buildable' => $buildable] arrays.
    public function getDisplayedProjects(City $city) : array
    {
        $this->city = $city;
        [$constructionDict, $requirementDict, $schemaDict] = [$this->gameDataService->getConstructionDict(), $this->gameDataService->getConstructionRequirementDict(), $this->gameDataService->getConstructionSchemaDict()];
        $displayedProjects = array();
        foreach(array_values($constructionDict) as $construction)
        {
            for($level=1; $level<6; $level++)
            {
                $requirement = $requirementDict[$construction->getName()][$level];
                $noRequiredBuilding = ($requirement->getRequiredConstruction() == null);
                $requiredBuildingLevelOK = ($noRequiredBuilding || $city->getBuildingLevel($requirement->getRequiredConstruction()->getName()) >= $requirement->getRequiredLevel());
                $previousLevelOK = ($level == 1 || $city->getBuildingLevel($construction->getName()) >= $level - 1);
                $alreadyBuilt = $city->getBuildingLevel($construction->getName()) >= $level;
                if ($requiredBuildingLevelOK && $previousLevelOK && !$alreadyBuilt)
                {
                    $buildable = $this->isABuildableConstruction($construction, $level);
                    array_push($displayedProjects, ['construction' => $construction, 'level' => $level, 'buildable' => $buildable]);
                }
            }
        }
        return $displayedProjects;
    }

    //returns an array of boolean (buildable, 'title' => titleSatisfied, 'resourceName1' => enoughResource1, 'resourceName2' => enoughResource2...)
    private function isABuildableConstruction(Constructions $construction, int $level) : array
    {
        $buildable = [0 => true];
        $requirementDict = $this->gameDataService->getConstructionRequirementDict();
        $requiredTitle = $requirementDict[$construction->getName()][$level]->getRequiredTitle();
        $currentTitle = $this->city->getLord()->getTitle();
        if ($requiredTitle != null)
        {
            $buildable['title'] = $currentTitle >= $requiredTitle;
            $buildable[0] &= $buildable['title']; 
        }
        foreach($construction->getOrderedSchemas()[$level] as $schema)
        {
            $resourceName = $schema->getResource()->getName();
            $possessedAmount = $this->getStorage($resourceName)->getAmount();
            $requiredAmount = $construction->getOrderedSchemas()[$level][$resourceName]->getAmount();
            if ($resourceName != 'remainingHammers')
            {
                $buildable[$resourceName] = $possessedAmount >= $requiredAmount;
                $buildable[0] &= $buildable[$resourceName];
            }
        }
        return $buildable;
    }

    //Consume the required resources and start the construction project
    public function startConstruction(City $city, string $constructionName, int $level) : bool
    {
        $this->city = $city;
        if($this->city->getConstructionProject() !== null)
            return false;

        [$constructionDict, $requirementDict] = [$this->gameDataService->getConstructionDict(), $this->gameDataService->getConstructionRequirementDict()];
        $construction = $constructionDict[$constructionName];
        $requirement = $requirementDict[$construction->getName()][$level];
        $this->city->setConstructionProject($construction);
        $this->city->setProjectLevel($level);
      
        $noRequiredBuilding = ($requirement->getRequiredConstruction() === null);
        $requiredBuildingLevelOK = ($noRequiredBuilding || $city->getBuildingLevel($requirement->getRequiredConstruction()->getName()) >= $requirement->getRequiredLevel());
        $previousLevelOK = ($level == 1 || $city->getBuildingLevel($construction->getName()) >= $level - 1);
        $alreadyBuilt = $city->getBuildingLevel($construction->getName()) >= $level;

        // TODO Also verify the title requirement!
        if (!$requiredBuildingLevelOK || !$previousLevelOK || $alreadyBuilt)
            return false;

        foreach($construction->getOrderedSchemas()[$level] as $schema)
        {
            $resourceName = $schema->getResource()->getName();
            $requiredAmount = $construction->getOrderedSchemas()[$level][$resourceName]->getAmount();
            if ($resourceName == 'remainingHammers')
            {
                $storage = $this->getStorage('remainingHammers');
                $storage->setAmount($requiredAmount);
            }
            elseif (in_array($resourceName, ['wheat', 'wood', 'gold', 'lin', 'iron', 'horse']))
            {
                $storage = $this->getStorage($resourceName);
                if($storage->getAmount() < $requiredAmount)
                    return false;

                $storage->setAmount($storage->getAmount() - $requiredAmount);
            }
            else
            {
                return false;
            }
        }

        return true;
    }

    //Give the required resources back, and stop the construction project
    public function cancelConstruction(City $city) : bool
    {
        $this->city = $city;
        $construction = $this->city->getConstructionProject();
        if($construction === null)
            return false;

        $level = $this->city->getProjectLevel();
        foreach($construction->getOrderedSchemas()[$level] as $schema)
        {
            $resourceName = $schema->getResource()->getName();
            $requiredAmount = $construction->getOrderedSchemas()[$level][$resourceName]->getAmount();
            if (in_array($resourceName, ['wheat', 'wood', 'gold', 'lin', 'iron', 'horse']))
            {
                $storage = $this->getStorage($resourceName);
                $storage->setAmount($storage->getAmount() + $requiredAmount);
            }
            elseif($resourceName =! 'remainingHammers')
            {
                return false;
            }
        }
        $this->city->setConstructionProject(null);
        return true;
    }

    //Get the matching Storage Object if it exists, else create it with amount 0
    public function getStorage(string $resourceName) : Storage
    {
        foreach($this->city->getStorages() as $storage) 
        {
            if($storage->getResource()->getName() == $resourceName)
            {
                return $storage;
            }
        }
        $storage = $this->gameDataService->createStorage($resourceName, 0);
        $this->city->addStorage($storage);
        return $storage;
    }

    //Get the matching Construction Object if it has been built, else create one with level 0
    private function getBuiltBuilding(string $buildingName) : ?BuiltBuildings
    {
        foreach($this->city->getBuiltBuildings() as $building)
        {
            if($building->getBuilding()->getName() == $buildingName)
            {
                return $building;
            }
        }
        $building = $this->gameDataService->createBuilding($buildingName, 0);
        $this->city->addBuiltBuilding($building);
        return $building;
    }

    //Get the current maxAmounts of each resources for the selected city
    public function getMaxStorages(City $city) : array
    {
        $this->city = $city;
        $maxStorageDict = $this->gameDataService->getMaxStorageDict();
        return ['wheat' => $maxStorageDict['wheat'][$city->getBuildingLevel('attic')]->getMaxAmount(),
                'gold' => 9999,
                'wood' => $maxStorageDict['wood'][$city->getBuildingLevel('hut')]->getMaxAmount(),
                'iron' => $maxStorageDict['iron'][$city->getBuildingLevel('forge')]->getMaxAmount(),
                'horse' => $maxStorageDict['horse'][$city->getBuildingLevel('stable')]->getMaxAmount(),
                'lin' => $maxStorageDict['lin'][$city->getBuildingLevel('factory')]->getMaxAmount(),
                    ];
    }

    //Returns an array filtered by resourceType (producedResourceName => [convertable, requiredResourceName => schemas object]) to be displayed in view
    public function getDisplayedConversionSchemas(City $city, string $filter) : array
    {
        $this->city = $city;
        $conversionSchemaDict = $this->gameDataService->getConversionSchemaDict(); $resourceDict = $this->gameDataService->getResourceDict();
        $displayedSchemas = [];
        foreach (array_keys($conversionSchemaDict) as $resourceName)
        {
            if ($resourceDict[$resourceName]->getResourceType() == $filter)
            {
                $buildingName = $conversionSchemaDict[$resourceName]['building'];
                for ($level=5; $level>0; $level--)
                {
                    foreach($conversionSchemaDict[$resourceName][$level] as $schema)
                    {
                        if ($this->city->getBuildingLevel($buildingName) == $schema->getRequiredLevel())
                        {
                            if(!in_array($resourceName, array_keys($displayedSchemas)))
                            {
                                $displayedSchemas[$resourceName] = [];
                            }
                            $displayedSchemas[$resourceName][0] = $this->isConvertable($resourceName, 1);
                            $displayedSchemas[$resourceName][$schema->getRequiredResource()->getName()] = $schema;        
                        }     
                    }
                }
            }  
        }
        return $displayedSchemas;
    }

    //Add the produced resource and remove the required ones
    public function convertResources(City $city, string $producedResourceName, int $amount) : bool
    {
        $this->city = $city;
        if ($amount > 0 && $this->isConvertable($producedResourceName, $amount))
        {
            $conversionSchemaDict = $this->gameDataService->getConversionSchemaDict();
            $buildingName = $conversionSchemaDict[$producedResourceName]['building'];
            $schemas = $conversionSchemaDict[$producedResourceName][$this->city->getBuildingLevel($buildingName)];
            foreach(array_keys($schemas) as $resourceName)
            {
                $storage = $this->getStorage($resourceName);
                $storage->setAmount($storage->getAmount() - $amount*$schemas[$resourceName]->getRequiredAmount());
            }
            $storage = $this->getStorage($producedResourceName);
            $storage->setAmount($storage->getAmount() + $amount*$schemas[$resourceName]->getProducedAmount());
            return true;
        }
        return false;
    } 

    //Returns true if the player possesses enough required resources
    private function isConvertable(string $producedResourceName, int $amount) : bool
    {
        $isConvertable = true;
        $conversionSchemaDict = $this->gameDataService->getConversionSchemaDict();
        $buildingName = $conversionSchemaDict[$producedResourceName]['building'];
        if ($this->city->getBuildingLevel($buildingName) == 0)
        {
            return false;
        }
        $schemas = $conversionSchemaDict[$producedResourceName][$this->city->getBuildingLevel($buildingName)];
        foreach(array_keys($schemas) as $resourceName)
        {
            $isConvertable &= ($this->city->getStorageCount($resourceName) >= $amount*$schemas[$resourceName]->getRequiredAmount());
        }
        return $isConvertable;
    }

    //Remove gold from stock and add general to DB
    public function recruitGeneral(City $city, string $name) : bool
    {
        // TODO uncomment that!
        /*
        $this->city = $city;
        $goldOK = ($this->city->getStorageCount('gold') >= 50*pow(2, count($city->getLord()->getGenerals())));
        $numberOfGeneralsOK =  (count($city->getLord()->getGenerals()) < $city->getBuildingLevel('headquarters'));
        if ($numberOfGeneralsOK && $goldOK)
        {
            $storage = $this->getStorage('gold');
            $storage->setAmount($storage->getAmount() - 50*pow(2, count($city->getLord()->getGenerals())));
            $general = new General();
            $general->setName($name);
            $general->setReputation(1);
            $general->setArmyContainer(new ArmyContainer());
            $general->setLocation($this->city->getCityNode());
            $this->city->getLord()->addGeneral($general);
            
            return true;
        }
        */
        return false;
    }


    //Switch on/off recruitment
    public function toggleRecruitment(City $city)
    {
        $city->setRecruiting(!$city->isRecruiting());
    }
}

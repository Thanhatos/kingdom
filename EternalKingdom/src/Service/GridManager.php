<?php

// src/Service/GridManager.php
namespace App\Service;

use App\Entity\City;
use App\Model\Tile;
use App\Model\SwapCandidate;
use App\Model\Alignment;
use App\Model\TileRespawnInfo;
use App\Model\GridChainReactionStep;

/**
 * Grid Manager aims to:
 * - initializes randomized grid
 * - Trigger swap and apply chain reaction (alignment, gravity, respawn)
 */
class GridManager
{
    const GRID_SIZE = 8;
    const CHEAT_USE_STATIC_GRID = false;
    const CHEAT_ALLOW_INVALID_SWAP = false;

    // Probability to spawn a specific element.
    const tileSpawningProbabilities = [0.22, 0.22, 0.22, 0.19, 0.005, 0.005, 0.14];
    const citizenProbabilityWhenFoodIsOK = 0.10;
    const armyProbabilityWhenGoldIsOK = 0.05;

    // Buffers for all calculations
    private $city;
    private $gridData;

    //Chain reaction information that is sent to the client after each swap
    private $steps;
    private $currentStep;

    /**
     * Initialize a grid
     * @return array
     */
    public function initGrid($city): array
    {
        $this->city = $city;

        if (GridManager::CHEAT_USE_STATIC_GRID)
        {
            $this->loadStaticGrid();
        }
        else
        {
            $this->randomizeGrid();
        }

        return $this->gridData;
    }

    /**
     * Tries to execute the specified swap
     * Returns if the swap is valid or not.
     * If the swap is valid, this triggers a chain reaction and stores every intermediary steps
     */
    public function swap(City &$city, SwapCandidate $swapCandidate): bool
    {
        $this->city = &$city;
        $this->gridData = array_map(function ($x)
        {
            return intval($x);
        }, str_split($city->getGrid()));

        if (!$this->isValidSwap($swapCandidate) && !GridManager::CHEAT_ALLOW_INVALID_SWAP)
        {
            return false;
        }

        [$row1, $col1] = [$swapCandidate->tile1->row, $swapCandidate->tile1->col];
        [$row2, $col2] = [$swapCandidate->tile2->row, $swapCandidate->tile2->col];
        $this->swapTiles($row1, $col1, $row2, $col2);
        $this->steps = array();
        $this->currentStep = 0;
        $this->chainReaction();

        return true;
    }

    /**
     * Find ValidAlignments after the Swap swapCandidate
     */
    private function findValidAlignments(SwapCandidate $swapCandidate): array
    {
        [$row1, $col1] = [$swapCandidate->tile1->row, $swapCandidate->tile1->col];
        [$row2, $col2] = [$swapCandidate->tile2->row, $swapCandidate->tile2->col];
        $this->swapTiles($row1, $col1, $row2, $col2);
        $alignment1 = $this->findAlignmentInBothDirections($row1, $col1);
        $alignment2 = $this->findAlignmentInBothDirections($row2, $col2);
        $validAlignments = array();
        foreach ([$alignment1, $alignment2] as $alignment)
        {
            if ($alignment->isValidAlignment())
            {
                array_push($validAlignments, $alignment);
            }
        }
        $this->swapTiles($row1, $col1, $row2, $col2); //We put the tiles back in their original place
        return $validAlignments;
    }

    /**
     * Swap the two specified tiles
     */
    private function swapTiles(int $r1, int $c1, int $r2, int $c2)
    {
        $buffer = $this->gridData[$r1 * GridManager::GRID_SIZE + $c1];
        $this->gridData[$r1 * GridManager::GRID_SIZE + $c1] = $this->gridData[$r2 * GridManager::GRID_SIZE + $c2];
        $this->gridData[$r2 * GridManager::GRID_SIZE + $c2] = $buffer;
    }

    /**
     * RECURSIVE CALL
     * Find if we still have alignments to clean in the grid
     * For each alignment found: remove all tiles, apply gravity & respawn new elements
     */
    private function chainReaction()
    {
        $allAlignments = $this->findAllValidAlignments();
        if (count($allAlignments) == 0)
        {
            $this->endChainReaction();
            return;
        }

        $this->steps[$this->currentStep] = new GridChainReactionStep();
        $this->clearAlignments($allAlignments);
        $this->applyGravity();
        $this->respawnElements();
        $this->currentStep++;
        $this->chainReaction();
    }

    /**
     * Find all the Alignments in the grid from bottom to top
     */
    private function findAllValidAlignments(): array
    {
        $gridCopy = $this->gridData;   //Buffer
        $alignments = array();
        for ($row = GridManager::GRID_SIZE - 1; $row >= 0; $row--)
        {
            for ($col = GridManager::GRID_SIZE - 1; $col >= 0; $col--)
            {
                $alignment = $this->findAlignmentInBothDirections($row, $col);
                if ($alignment->isValidAlignment())
                {
                    array_push($alignments, $alignment);
                    foreach ($alignment->tiles as $tile)
                    {
                        $this->gridData[GridManager::GRID_SIZE * $tile->row + $tile->col] = -1;
                    }
                }
            }
        }
        $this->gridData = $gridCopy; //Restore Grid, TODO : Put Grid in argument to low-level functions
        return $alignments;
    }

    /**
     * Empty all specified alignments' tiles
     */
    private function clearAlignments(array $alignments)
    {
        foreach ($alignments as $alignment)
        {
            $this->steps[$this->currentStep]->addAlignment($alignment);
            foreach ($alignment->tiles as $tile)
            {
                $this->gridData[$tile->row * GridManager::GRID_SIZE + $tile->col] = -1;
            }
        }
    }

    /**
     * Apply gravity in the whole grid.
     * For each column, move all occupied tiles at the lowest possible empty position
     */
    private function applyGravity()
    {
        for ($col = 0; $col < GridManager::GRID_SIZE; $col++)
        {
            $currentEmptyRow = -1;
            for ($row = GridManager::GRID_SIZE - 1; $row >= 0; $row--)
            {
                $elem = $this->getElementAt($row, $col);
                if ($currentEmptyRow == -1 && $elem == -1)
                {
                    $currentEmptyRow = $row;
                }
                else if ($currentEmptyRow != -1 && $elem != -1)
                {
                    $this->swapTiles($row, $col, $currentEmptyRow, $col);
                    $currentEmptyRow--;
                }
            }
        }
    }

    /**
     * Respawns a new element in all empty tiles of the grid and stores all the respawn information
     */
    private function respawnElements()
    {
        for ($row = 0; $row < GridManager::GRID_SIZE; $row++)
        {
            for ($col = 0; $col < GridManager::GRID_SIZE; $col++)
            {
                if ($this->getElementAt($row, $col) == -1)
                {
                    $elem = $this->getRandomElement();
                    $this->gridData[$row * GridManager::GRID_SIZE + $col] = $elem;
                    $this->steps[$this->currentStep]->addRespawn(new TileRespawnInfo($row, $col, $elem));
                }
            }
        }
    }

    /**
     * Triggered once a grid chain reaction ended
     */
    private function endChainReaction()
    {
        if (empty($this->findAllValidSwaps()))
        {
            $this->randomizeGrid();
            $string = $this->convertGridToString($this->gridData);
            $this->city->setGrid($string);
            array_push($this->steps, $string);
        }
        else
        {
            $string = $this->convertGridToString($this->gridData);
            $this->city->setGrid($string);
        }
    }

    /**
     * Returns valid swaps in the whole grid
     */
    private function findAllValidSwaps(): array
    {
        $allValidSwaps = array();
        for ($row = 0; $row < GridManager::GRID_SIZE; $row += 2)
        {
            for ($col = 0; $col < GridManager::GRID_SIZE; $col++)
            {
                $swapCandidate = new SwapCandidate(new Tile($row, $col), new Tile($row + 1, $col));
                if ($this->IsValidSwap($swapCandidate))
                {
                    array_push($allValidSwaps, $swapCandidate);
                }
                $swapCandidate = new SwapCandidate(new Tile($row, $col), new Tile($row - 1, $col));
                if ($this->IsValidSwap($swapCandidate))
                {
                    array_push($allValidSwaps, $swapCandidate);
                }
            }
        }
        for ($col = 0; $col < GridManager::GRID_SIZE; $col += 2)
        {
            for ($row = 0; $row < GridManager::GRID_SIZE; $row++)
            {
                $swapCandidate = new SwapCandidate(new Tile($row, $col), new Tile($row, $col + 1));
                if ($this->IsValidSwap($swapCandidate))
                {
                    array_push($allValidSwaps, $swapCandidate);
                }
                $swapCandidate = new SwapCandidate(new Tile($row, $col), new Tile($row, $col - 1));
                if ($this->IsValidSwap($swapCandidate))
                {
                    array_push($allValidSwaps, $swapCandidate);
                }
            }
        }
        return $allValidSwaps;
    }

    /**
     * Returns if the specified swap is valid
     */
    private function isValidSwap(SwapCandidate $swapCandidate): bool
    {
        if ($this->isTileInGrid($swapCandidate->tile1) && $this->isTileinGrid($swapCandidate->tile2))
        {
            $validAlignments = $this->findValidAlignments($swapCandidate);
            if (count($validAlignments) > 0)
            {
                return true;
            }
        }

        return false;
    }

    private function isTileInGrid(Tile $tile): bool
    {
        return 0 <= $tile->row && $tile->row < GridManager::GRID_SIZE
            && 0 <= $tile->col && $tile->col < GridManager::GRID_SIZE;
    }

    /**
     * Load a grid with static values
     */
    private function loadStaticGrid()
    {
        // Please make sure, this grid has no alignment!
        // Note: This setup helps to reproduce easily some bugs.

        // $this->gridData =
        // [
        //     3, 1, 2, 3, 6, 3, 3, 6,
        //     5, 5, 0, 6, 0, 2, 3, 3,
        //     2, 2, 3, 3, 2, 6, 1, 0,
        //     2, 2, 1, 1, 2, 6, 3, 2,
        //     6, 5, 4, 3, 0, 1, 3, 5,
        //     0, 4, 5, 6, 5, 0, 1, 0,
        //     3, 6, 1, 3, 1, 1, 0, 5,
        //     0, 1, 3, 3, 0, 4, 3, 6,
        // ];

        $this->gridData =
            [
                6, 6, 0, 1, 2, 3, 4, 5,
                0, 1, 2, 3, 4, 5, 5, 6,
                6, 6, 0, 1, 2, 3, 4, 5,
                0, 1, 2, 3, 4, 5, 6, 6,
                6, 6, 0, 1, 2, 3, 4, 5,
                0, 1, 2, 3, 4, 5, 6, 6,
                6, 6, 0, 1, 2, 3, 4, 5,
                0, 1, 2, 3, 4, 5, 6, 6,
            ];
    }

    /**
     * Randomizes the grid values while avoiding creating any alignments
     */
    private function randomizeGrid()
    {
        $this->gridData = array_fill(0, 64, -1);

        for ($row = 0; $row < GridManager::GRID_SIZE; $row++)
        {
            for ($col = 0; $col < GridManager::GRID_SIZE; $col++)
            {
                do
                {
                    $this->gridData[$row * GridManager::GRID_SIZE + $col] = $this->getRandomElement();
                    $isCreatingAlignment = $this->findAlignmentInBothDirections($row, $col)->isValidAlignment();
                } while ($isCreatingAlignment);
            }
        }
    }

    /**
     * Returns the next element to generate in the grid
     * @return int 0 to 7
     */
    private function getRandomElement(): int
    {
        $probs = GridManager::tileSpawningProbabilities;
        $delta = 0;
        $modifiedIndexes = [];
        if ($this->city->getStorageCount('gold') > 0.5*$this->city->getPopulationCount()**2)
        {
        	$delta += GridManager::armyProbabilityWhenGoldIsOK - $probs[4]; 
            $probs[4] = GridManager::armyProbabilityWhenGoldIsOK; 
            array_push($modifiedIndexes, 4);
            
        }
        if ($this->city->getStorageCount('wheat') > $this->city->getPopulationCount()**2)
        {
            $delta += GridManager::citizenProbabilityWhenFoodIsOK - $probs[5];   
        	$probs[5] = GridManager::citizenProbabilityWhenFoodIsOK;   
            array_push($modifiedIndexes, 5);
        }
        if (count($modifiedIndexes) > 0)
        {
            for($index = 0; $index < count($probs); $index++)
            {
                if(in_array($index, $modifiedIndexes))
                {
                    $probs[$index] -= $delta/(count($probs)-count($modifiedIndexes));
                }
            } 
        }
        $rand = mt_rand() / mt_getrandmax();
        $cumulatedProbability = 0;
        for ($i = 0; $i < count($probs); $i++)
        {
            $cumulatedProbability += $probs[$i];
            if ($rand < $cumulatedProbability)
            {
                break;
            }
        }

        return $i;
    }

    /**
     * Get the element at the specified coordinates
     * @param $row
     * @param $col
     * @return int
     */
    private function getElementAt($row, $col): int
    {
        return $this->gridData[$row * GridManager::GRID_SIZE + $col];
    }

    /**
     * Try to find a list of tiles with a valid alignment.
     * Prioritize the longer alignment or the horizontal if length is the same.
     * @param $row
     * @param $col
     * @return Alignment
     */
    private function findAlignmentInBothDirections($row, $col): Alignment
    {
        $elem = $this->getElementAt($row, $col);

        // Probably clean by another alignment // BE CARE!
        if ($elem === -1)
        {
            return Alignment::InvalidAlignment();
        }

        $alignmentH = $this->findHorizontalAlignment($row, $col);
        $alignmentV = $this->findVerticalAlignment($row, $col);

        return $this->getBestAlignment($alignmentH, $alignmentV);
    }

    /**
     * Returns a horizontal alignment of the same tile element from the specified coordinates
     * @param int $row
     * @param int $col
     * @return Alignment
     */
    private function findHorizontalAlignment(int $row, int $col): Alignment
    {
        $elementToSearch = $this->getElementAt($row, $col);

        if ($elementToSearch === -1)
            return Alignment::InvalidAlignment();

        $tiles = array();
        array_push($tiles, new Tile($row, $col));

        for ($j = $col + 1; $j < GridManager::GRID_SIZE; $j++)
        {
            if ($this->getElementAt($row, $j) !== $elementToSearch)
                break;

            array_push($tiles, new Tile($row, $j));
        }

        for ($j = $col - 1; $j >= 0; $j--)
        {
            if ($this->getElementAt($row, $j) !== $elementToSearch)
                break;

            array_push($tiles, new Tile($row, $j));
        }

        return new Alignment($tiles, $elementToSearch);
    }

    /**
     * Returns a vertical alignment of the same tile element from the specified coordinates
     * @param $row
     * @param $col
     * @return Alignment
     */
    private function findVerticalAlignment($row, $col): Alignment
    {
        $elementToSearch = $this->getElementAt($row, $col);

        if ($elementToSearch === -1)
            return Alignment::InvalidAlignment();

        $tiles = array();
        array_push($tiles, new Tile($row, $col));

        for ($i = $row + 1; $i < GridManager::GRID_SIZE; $i++)
        {
            if ($this->getElementAt($i, $col) !== $elementToSearch)
                break;

            array_push($tiles, new Tile($i, $col));
        }

        for ($i = $row - 1; $i >= 0; $i--)
        {
            if ($this->getElementAt($i, $col) !== $elementToSearch)
                break;

            array_push($tiles, new Tile($i, $col));
        }

        return new Alignment($tiles, $elementToSearch);
    }

    /**
     * Returns the best valid alignment between the two specified
     * @param $alignmentH
     * @param $alignmentV
     * @return Alignment
     */
    private function getBestAlignment($alignmentH, $alignmentV): Alignment
    {
        $bestAlignment = $alignmentH->getLength() >= $alignmentV->getLength() ? $alignmentH : $alignmentV;

        if ($bestAlignment->getLength() >= 3)
        {
            return $bestAlignment;
        }

        return Alignment::InvalidAlignment();
    }

    /**
     * Convert a grid data to a single string of 64 characters
     * @param array $grid
     * @return string
     */
    static public function convertGridToString(array $grid): string
    {
        return implode($grid);
    }

    /**
     * Returns all info about the last chain reaction triggered following a swap
     */
    public function getChainReactionSteps(): array
    {
        return $this->steps;
    }
}

<?php

namespace App\Model;
use App\Model\Alignment;
use App\Model\TileRespawnInfo;

//Stores what happens after the player swapped two tiles
class GridChainReactionStep
{
	public $alignments;
	public $spawns;

	public function __construct()
	{
		$this->alignments = array();
		$this->spawns = array();
	}
	public function addAlignment(Alignment $alignment)
	{
		array_push($this->alignments, $alignment);
	}

	public function addRespawn(TileRespawnInfo $spawn)
	{
		array_push($this->spawns, $spawn);
	}

}

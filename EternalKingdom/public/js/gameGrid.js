// KINGDOM MATCH3 - ETERNAL TWIN TEAM - V2 - Bibni

/* Next things to do
 *
 * Polish the alignment reward animation (Queue with opacity over time)
 *
 * TODO Swap hint should highlight the element that create the alignement
 *
 * Predict the first chain reaction step!
 * Update the page with the chain reaction gains!!! We miss armies
 *
 * POLISH all animations (better filter)
 * POLISH Tweak speed of each step
 * POLISH swap feedback
 *
 * Add history logs (temporary and maybe useless...)
 */

//#region enum

//region Data definition

class Tile
{
    constructor(row, col)
    {
        this.row = row;
        this.col = col;
    }
}

class Alignment
{
    constructor(tiles, element, gain = 0)
    {
        this.tiles = tiles;
        this.element = element;
        this.gain = gain;
    }

    isValid()
    {
        return this.tiles.length >= 3 && this.element !== -1;
    };
}

class InvalidAlignment extends Alignment
{
    constructor()
    {
        super([], -1);
    }
}

class RespawnInfo
{
    constructor(row, col, element)
    {
        this.row = row;
        this.col = col;
        this.element = element;
    }
}

class Movement
{
    constructor(startRow, startCol, destRow, destCol)
    {
        this.row = startRow;
        this.col = startCol;
        this.startX = gridViewData.tilePositions[startRow][startCol][0];
        this.startY = gridViewData.tilePositions[startRow][startCol][1];
        this.destX = gridViewData.tilePositions[destRow][destCol][0];
        this.destY = gridViewData.tilePositions[destRow][destCol][1];
    }
}

class TileFilter
{
    constructor(filter, dontClear)
    {
        this.filter = filter;
        this.dontClear = dontClear;
    }
}

/* Animation */

// TODO Replace all ComputeAnimation by a class function!
class BaseAnimation
{
    constructor(id, duration)
    {
        this.id = id;
        this.duration = duration;
    }
}

class SwapAnimation extends BaseAnimation
{
    constructor(a_row1, a_col1, a_row2, a_col2)
    {
        super(0, 125);

        this.move1 = new Movement(a_row1, a_col1, a_row2, a_col2);
        this.move2 = new Movement(a_row2, a_col2, a_row1, a_col1);

        // TODO Write the nested loop to deep copy this array!
        this.gridBuffer = JSON.parse(JSON.stringify(gridViewData.tilePositions));
    }
}

class ClearAnimation extends BaseAnimation
{
    constructor(alignments)
    {
        super(1, 300);

        this.alignments = alignments;
    }
}

// TODO Refactor Gravity and swap animation into MovementAnimation?
class GravityAnimation extends BaseAnimation
{
    constructor(a_moves, a_respawnInfos)
    {
        super(2, 125);

        this.moves = a_moves;
        this.respawnInfos = a_respawnInfos;
        this.gridBuffer = JSON.parse(JSON.stringify(gridViewData.tilePositions));
    }
}

class RespawnAnimation extends BaseAnimation
{
    constructor(respawnsInfo)
    {
        super(3, 300);

        this.respawnsInfo = respawnsInfo;
    }
}

class ResetAnimation extends BaseAnimation
{
    constructor(gridData)
    {
        super(4, (gridSize * 2 * 100));

        this.gridData   = gridData; // new data applied to the grid
        this.diagonal   = 0;        // Which diagonal do we currently fade?
        this.reverse    = false;    // Do we play animation reverse?
    }
}

// TODO Create a parent class Event to just trigger something without requiring duration!
class ChainReactionEndAnimation extends BaseAnimation
{
    constructor(resources)
    {
        super(5, 1);
        this.resources = resources;
    }
}

// List of all waiting time used in the project here
const waitingTimeBetweenChainSteps = 300;
const waitingTimeBeforeGravity = 100;

class WaitingAnimation extends BaseAnimation
{
    constructor(time)
    {
        super(6, time);
    }
}

class RenderView
{
    constructor(canvas)
    {
        this.canvas = canvas;
        this.context = canvas.getContext("2d");

        let canvasSize = gridViewData.tileSize * gridSize;
        this.canvas.height = canvasSize;
        this.canvas.width = canvasSize;
    }
}

//#endregion

//#region Static variables
const ResourceType = {
    FOOD   : 0,
    GOLD   : 1,
    WOOD   : 2,
    HAMMER : 3,
    ARMY   : 4,
    CITIZEN: 5,
    TURN   : 6,
    IRON   : 7,
    LINEN  : 8,
    HORSE  : 9,
}
const CitizenType = {
    FARMER    : 0,
    LUMBERJACK: 1,
    WORKER    : 2,
    MERCHANT  : 3,
    RECRUITER : 4,
    PENDING   : 5,
}

const tileTypes = ['wheat', 'gold', 'wood', 'hammer', 'army', 'citizen', 'replay'];
const imagesSrc = {
    'wheat'  : 'img/icons/res_wheat.png',
    'gold'   : 'img/icons/res_gold.png',
    'wood'   : 'img/icons/res_wood.png',
    'hammer' : 'img/icons/res_build.png',
    'army'   : 'img/icons/res_army.png',
    'citizen': 'img/icons/res_citizen.png',
    'replay' : 'img/icons/res_rec.png',
    // swap feedback
    'vswap': 'img/icons/vswap.png',
    'hswap': 'img/icons/hswap.png',
}

const hoverFilter   = "drop-shadow(0px 0px 2px #000) brightness(1.2) contrast(1.5)";
const hintFilter    = "drop-shadow(0px 0px 4px #ccc) brightness(1.4) contrast(1.75)";
const swapFeedbackOffset = 18.125;

const dirDebugNames = ["Top", "Right", "Bottom", "Left"];

// TODO Prevent to play on backend!
const cheatInfiniteTurn = true;

//#endregion

//#region Dynamic variables
let KingdomLoop =
{
    stopMain  : null,
    lastRender: -1,
};

let cityUrl;

// Model
let gridSize = 8;
let tiles = [];

let city =
{
    resources         : [0, 0, 0, 0, 0, 0],
    maxResources      : [100, 9999, 100, 10, 10, 10],
    populations       : [0, 0, 0, 0, 0, 0],
    armies            : [0, 0, 0, 0, 0, 0], // Probably need more
    turn              : 100,
    canCreateRecruiter: false,
}

// View
let loadedImages = {};          // List of images that can be used dynamically
let currentTileEffects = {};    // List of current effects applied on tiles

let gridView;
let frontView;
let noTurnFeedback;

let gridViewData =
{
    tilePositions      : [],    // tile view (coordinates X and Y)
    tilePositionsBuffer: [],    // Used to restore the whole grid in a previous state (useful for animations)
    tileSize           : 36,    // size of a tile (not the size of a tile image)
}

// Interaction
let hoveredRow = -1, hoveredCol = -1;
let rowToSwap = -1, colToSwap = -1;
let invalidSwapCount = 0;

// Main loop
let elapsedTime = 0;
let lastUpdate = 0;

// Animation
let animationQueue = [];
let isPlayingAnimation = false;
let currentAnim = undefined;
let animTime = 0;

//#endregion

//#region Gameplay

//#region Populations

function LoadPopulationFromWebPage()
{
    for (let i = 0; i < city.populations.length; i++)
    {
        LoadPopulationCount(i);
    }

    if (city.populations[CitizenType.PENDING] > 0)
    {
        RefreshAllPopulationSlots();
    }
}

// Read the value from the page to init the specified citizen population
function LoadPopulationCount(a_citizenType)
{
    let populationView = GetPopulationView(a_citizenType);
    city.populations[a_citizenType] = parseInt(populationView.innerHTML);
}

function SetPopulationCount(a_citizenType, a_amount)
{
    if(city.populations[a_citizenType] === a_amount)
        return;

    city.populations[a_citizenType] = a_amount;
    UpdatePopulationView(a_citizenType);

    if(a_citizenType === CitizenType.PENDING)
    {
        RefreshAllPopulationSlots();
    }
}

function ConvertCitizen(a_citizenType)
{
    city.populations[a_citizenType]++;
    city.populations[CitizenType.PENDING]--;

    UpdatePopulationView(a_citizenType);
    UpdatePopulationView(CitizenType.PENDING);

    if (city.populations[CitizenType.PENDING] === 0)
    {
        RefreshAllPopulationSlots();
    }
}

function UpdatePopulationView(a_citizenType)
{
    let populationView = GetPopulationView(a_citizenType);
    populationView.innerHTML = city.populations[a_citizenType];
}

function RefreshAllPopulationSlots()
{
    for (let i = 0; i < city.populations.length; i++)
    {
        RefreshPopulationSlot(i);
    }
}

function RefreshPopulationSlot(a_citizenType)
{
    let populationView = GetPopulationView(a_citizenType);

    // if we can add population, put list item as disable if they have 0 population (but permits to add one)

    if (city.populations[CitizenType.PENDING] > 0)
    {
        if (a_citizenType !== CitizenType.PENDING)
        {
            // Ignore recruiter if not available
            if (a_citizenType === CitizenType.RECRUITER && !city.canCreateRecruiter)
            {
                return;
            }

            let button = populationView.parentNode.parentNode.querySelector("a");
            if (!button)
            {
                let button = document.createElement("a");
                button.classList.add("addpeople");
                button.addEventListener("click", function ()
                {
                    RequestCitizenConversion(a_citizenType);
                });

                // We have to add this before the list item!
                populationView.parentNode.parentNode.prepend(button);
            }
        }

        populationView.parentNode.classList.remove("empty");
        populationView.parentNode.classList.remove("disable");
    }
    else
    {
        let button = populationView.parentNode.parentNode.querySelector("a");
        if (button)
        {
            button.remove();
        }

        if (city.populations[a_citizenType] === 0)
        {
            populationView.parentNode.classList.add("disable");
        }
        else
        {
            populationView.parentNode.classList.remove("disable");
        }
    }
}

function GetPopulationView(a_citizenType)
{
    return document.querySelectorAll(".population.items .number")[a_citizenType];
}

function RequestCitizenConversion(a_selectedCitizenType)
{
    let payload = {
        citizenId: a_selectedCitizenType,
    }

    ServerPost('/convertCitizen', payload, HandleConversionResponse);

    ConvertCitizen(a_selectedCitizenType);
}

function HandleConversionResponse(data)
{
    alert(data);
}

//#endregion

//#region Resources

function LoadResourcesFromWebPage()
{
    for (let i = 0; i < city.resources.length; i++)
    {
        LoadResourceCount(i);
    }
}

// Read the value from the page to init the resource stock
function LoadResourceCount(a_resource)
{
    let resourceView = document.querySelectorAll(".rescity .n")[a_resource];
    city.resources[a_resource] = parseInt(resourceView.innerHTML);

    city.maxResources[a_resource] = parseInt(resourceView.dataset.max);

    PostStockCountChanged(a_resource, resourceView);
}

function SetResourceCount(a_resource, a_value)
{
    city.resources[a_resource] = a_value;

    let resourceView = document.querySelectorAll(".rescity .n")[a_resource];
    resourceView.innerHTML = a_value.toString();

    PostStockCountChanged(a_resource, resourceView);
}

function PostStockCountChanged(a_resource, a_resourceView)
{
    if(a_resourceView === undefined)
        return;

    if (city.resources[a_resource] < 20)
    {
        /* Feedback that a stock is low (only for the first 3 resources) */
        if (a_resource < 3)
        {
            a_resourceView.classList.add("need");
        }
    }
    else if(city.resources[a_resource] === city.maxResources[a_resource])
    {
        a_resourceView.classList.add("maxed");
    }
    else
    {
        a_resourceView.classList.remove("need");
        a_resourceView.classList.remove("maxed");
    }
}

//#endregion

//#region Grid

//#region View

// Load several server data stored in the webpage
function LoadGridDataFromWebPage()
{
    let serverData = document.querySelector('.ServerData');

    if (serverData != null)
    {
        let gridData = serverData.dataset.grid;
        InitGridWithString(gridData);

        let lordId = serverData.dataset.lordid;
        cityUrl = "/city/" + lordId;
    }
    else
    {
        console.error("Failed to find grid data from the webpage !");
    }
}

/**
 * Inits the grid thanks to the specified string
 */
function InitGridWithString(gridData)
{
    //console.log("Server Grid DATA: " + gridData);

    for (let row = 0; row < gridSize; row++)
    {
        tiles[row] = new Array(gridSize);
    }

    for (let i = 0; i < gridData.length; i++)
    {
        let row = Math.floor(i / 8);
        tiles[row][i % 8] = parseInt(gridData[i]);
    }
}

function InitGridView()
{
    let puzzleFrame = document.querySelector("#puzzleframe");

    gridView = new RenderView(puzzleFrame.querySelector("#puzzleCanvas"));
    frontView = new RenderView(puzzleFrame.querySelector("#frontCanvas"));
    noTurnFeedback = puzzleFrame.querySelector(".puzzleturns");
    frontView.context.font = "20px serif";
    frontView.context.textAlign = "center";

    // Init all tile initial positions
    for (let row = 0; row < gridSize; row++)
    {
        gridViewData.tilePositions[row] = new Array(gridSize);

        for (let col = 0; col < gridSize; col++)
        {
            // X and Y
            gridViewData.tilePositions[row][col] = new Array(gridSize);
            gridViewData.tilePositions[row][col][0] = 2 + gridViewData.tileSize * col;
            gridViewData.tilePositions[row][col][1] = 2 + gridViewData.tileSize * row;
        }
    }
}

function RenderGrid()
{
    ClearRenderView(gridView);

    for (let row = 0; row < gridSize; row++)
    {
        for (let col = 0; col < gridSize; col++)
        {
            gridView.context.filter = GetTileEffect(GetTileUID(row, col));
            DrawTileAt(gridViewData.tilePositions[row][col][0], gridViewData.tilePositions[row][col][1], tiles[row][col]);
        }
    }
}

function ClearRenderView(a_view)
{
    if(a_view)
        a_view.context.clearRect(0, 0, gridViewData.tileSize * gridSize, gridViewData.tileSize * gridSize);
}

// Draw one tile at the specified position
function DrawTileAt(x, y, element)
{
    if (element === -1 || element >= tileTypes.length)
    {
        return;
    }

    gridView.context.drawImage(
        loadedImages[tileTypes[element]],
        x,
        y,
    );
}

function DrawAlignmentReward(a_avgRow, a_avgCol, a_element, a_gain)
{
    let x = a_avgCol * gridViewData.tileSize + 2;
    let y = a_avgRow * gridViewData.tileSize + 2;

    //frontView.context.filter = "opacity(" + minusT + ")";
    frontView.context.drawImage(
        loadedImages[tileTypes[a_element]],
        x - gridViewData.tileSize * 0.5,
        y,
        30,
        30
    );

    let text;
    if(a_gain > 0)
        text = "+" + a_gain.toString();
    else
        text = a_gain.toString();

    DrawOutlinedText(
        text,
        x + gridViewData.tileSize * 0.75,
        y + gridViewData.tileSize * 0.7,
        frontView.context
    );
}

function DrawOutlinedText(text, x, y, ctx)
{
    ctx.fillStyle = "black";
    ctx.lineWidth = 4;

    ctx.strokeText(
        text,
        x,
        y,
    );

    ctx.lineWidth = 1;
    ctx.fillStyle = "white";

    ctx.fillText(
        text,
        x,
        y,
    );
}

function GetTileEffect(tileUID)
{
    if (currentTileEffects[tileUID] != null)
    {
        return currentTileEffects[tileUID].filter;
    }
    return "none";
}

function SetTileEffect(tileUID, filter, dontClear = false)
{
    if (currentTileEffects[tileUID] != null && currentTileEffects[tileUID].dontClear)
        return;

    currentTileEffects[tileUID] = new TileFilter(filter, dontClear);
}

function TryToClearTileEffect(tileUID)
{
    if (currentTileEffects[tileUID] != null && !currentTileEffects[tileUID].dontClear)
    {
        delete currentTileEffects[tileUID];
    }
}

function ClearTileEffect(tileUID)
{
    if (currentTileEffects[tileUID] != null)
    {
        delete currentTileEffects[tileUID];
    }
}

function ClearAllTileEffects(clearAll = false)
{
    if(clearAll)
    {
        for (let effectUID in currentTileEffects)
            ClearTileEffect(effectUID);
    }
    else
    {
        for (let effectUID in currentTileEffects)
        {
            TryToClearTileEffect(effectUID);
        }
    }
}

function GetTileUID(row, col)
{
    return row * gridSize + col;
}

//#endregion

//#region Model

function IsValidSwap(rowA, colA, rowB, colB)
{
    if (AreCoordinatesInGrid(rowA, colA) && AreCoordinatesInGrid(rowB, colB))
    {
        let validAlignments = FindValidAlignments(rowA, colA, rowB, colB);
        return validAlignments.length > 0;
    }

    return false;
}

function AreCoordinatesInGrid(row, col)
{
    return row >= 0 && col >= 0 && row < gridSize && col < gridSize;
}

/**
 * Find ValidAlignments after the Swap swapCandidate
 */
function FindValidAlignments(a_rowA, a_colA, a_rowB, a_colB)
{
    SwapTiles(a_rowA, a_colA, a_rowB, a_colB);

    let alignments = [];

    let alignmentA = FindAlignmentInBothDirections(a_rowA, a_colA);
    if (alignmentA !== undefined && alignmentA.isValid())
    {
        alignments.push(alignmentA);
    }

    let alignmentB = FindAlignmentInBothDirections(a_rowB, a_colB);
    if (alignmentB !== undefined && alignmentB.isValid())
    {
        alignments.push(alignmentB);
    }

    SwapTiles(a_rowA, a_colA, a_rowB, a_colB); //We put the tiles back in their original place
    return alignments;
}

/**
 * Return alignments found from the two specified tile coordinates
 */
function FindAlignmentInBothDirections(a_row, a_col)
{
    let elem = tiles[a_row][a_col];

    if (elem === -1)
    {
        return new InvalidAlignment();
    }

    let alignmentH = FindHorizontalAlignment(a_row, a_col);
    let alignmentV = FindVerticalAlignment(a_row, a_col);

    return GetBestAlignment(alignmentH, alignmentV);
}

/**
 * Returns an horizontal alignment of the same tile type
 * alignment is searched from the tile [row, col]
 */
function FindHorizontalAlignment(a_row, a_col)
{
    let elem = tiles[a_row][a_col];
    if (elem === -1)
    {
        return new InvalidAlignment();
    }

    let foundTiles = [];
    foundTiles.push(new Tile(a_row, a_col)); // we push self.

    for (let x = a_col + 1; x < gridSize; x++)
    {
        if (tiles[a_row][x] === undefined)
        {
            break;
        }

        if (tiles[a_row][x] !== elem)
        {
            break;
        }
        else
        {
            foundTiles.push(new Tile(a_row, x));
        }
    }

    for (let x = a_col - 1; x >= 0; x--)
    {
        if (tiles[a_row][x] !== elem)
        {
            break;
        }
        else
        {
            foundTiles.push(new Tile(a_row, x));
        }
    }

    return new Alignment(foundTiles, elem);
}

/**
 * Returns a vertical alignment of the same tile type
 * alignment is searched from the tile [row, col]
 */
function FindVerticalAlignment(a_row, a_col)
{
    let elem = tiles[a_row][a_col];
    if (elem === -1)
    {
        return new InvalidAlignment();
    }

    let foundTiles = [];
    foundTiles.push(new Tile(a_row, a_col)); // we push self.

    for (let y = a_row + 1; y < gridSize; y++)
    {
        if (tiles[y] === undefined)
        {
            break;
        }

        if (tiles[y][a_col] !== elem)
        {
            break;
        }
        else
        {
            foundTiles.push(new Tile(y, a_col));
        }
    }

    for (let y = a_row - 1; y >= 0; y--)
    {
        if (tiles[y][a_col] !== elem)
        {
            break;
        }
        else
        {
            foundTiles.push(new Tile(y, a_col));
        }
    }

    return new Alignment(foundTiles, elem);
}

/**
 * Returns the best alignment between the two specified.
 * Only returns a valid alignment if the size is greater than 3!
 */
function GetBestAlignment(a_alignmentH, a_alignmentV)
{
    let bestAlignment = a_alignmentH.tiles.length >= a_alignmentV.tiles.length ? a_alignmentH : a_alignmentV;

    if (bestAlignment.tiles.length >= 3)
    {
        return bestAlignment;
    }

    return new InvalidAlignment();
}

function SwapTiles(rowA, colA, rowB, colB)
{
    let buffer = tiles[rowB][colB];
    tiles[rowB][colB] = tiles[rowA][colA];
    tiles[rowA][colA] = buffer;
}


/*
 * Verify if the grid contains at least one feasible alignment with one swap
 * If any found : display an hint to the player.
 * Else reinitialize the grid.
 */
function EvaluateGrid()
{
    let tileCandidates = [];
    for (let r = 0; r < gridSize; r += 2)
    {
        for (let c = 0; c < gridSize; c++)
        {
            let candidate = EvaluateSwap(r, c, r + 1, c);
            if(candidate)
                tileCandidates.push(candidate);

            candidate = EvaluateSwap(r, c, r - 1, c);
            if(candidate)
                tileCandidates.push(candidate);
        }
    }

    for (let c = 0; c < gridSize; c += 2)
    {
        for (let r = 0; r < gridSize; r++)
        {
            let candidate = EvaluateSwap(r, c, r, c + 1);
            if(candidate)
                tileCandidates.push(candidate);

            candidate = EvaluateSwap(r, c, r, c - 1);
            if(candidate)
                tileCandidates.push(candidate);
        }
    }

    if (tileCandidates.length > 0)
    {
        let tileHint = tileCandidates[GetRandomInRange(tileCandidates.length)];
        SetTileEffect(GetTileUID(tileHint.row, tileHint.col), hintFilter, true);
        RenderGrid();
    }
}

function EvaluateSwap(rowA, colA, rowB, colB)
{
    let tile = null;
    if(AreCoordinatesInGrid(rowA, colA) && AreCoordinatesInGrid(rowB, colB))
    {
        let alignments = FindValidAlignments(rowA, colA, rowB, colB);
        if (alignments.length > 0)
        {
            if (alignments[0].element === tiles[rowA][colA])
                tile = new Tile(rowA, colA);
            else
                tile = new Tile(rowB, colB);
        }
    }
    return tile;
}

//#endregion

//#endregion

function LoadRelevantCityBuildings()
{
    // TODO We maybe need to load all buildings

    let barracksBuilt = document.querySelector('*[data-building-name="barracks"]');
    city.canCreateRecruiter = barracksBuilt !== null && barracksBuilt.dataset.buildingLevel >= 2;
}

function SetTurnCount(a_turnCount)
{
    if(a_turnCount === 0)
        return;

    let turnsHtml = document.querySelector("#counter");

    turnsHtml.innerHTML = a_turnCount.toString();

    if(a_turnCount <= 0 && !cheatInfiniteTurn)
    {
        FeedbackNoTurnAvailable(true);
    }
}

function FeedbackNoTurnAvailable(a_hasNoTurn)
{
    if(a_hasNoTurn)
    {
        gridView.canvas.classList.add("disable");
        noTurnFeedback.classList.remove("disable");
    }
    else
    {
        gridView.canvas.classList.remove("disable");
        noTurnFeedback.classList.add("disable");
    }
}

function UpdateRemainingHammer(a_hammer)
{
    let remainingHammerHTML = document.querySelector(".firstp strong");
    if(remainingHammerHTML === null)
        return;

    if(a_hammer <= 0)
    {
        location.reload();
        return;
    }

    remainingHammerHTML.innerHTML = a_hammer.toString();
}

//#endregion

//#region Interaction

// Enable interaction with grid
function InitEventListeners()
{
    gridView.canvas.addEventListener('click', OnClickOnGrid);
    gridView.canvas.addEventListener('mouseenter', onMouseEnterGrid);
    gridView.canvas.addEventListener('mousemove', onMouseMoveOverGrid);
    gridView.canvas.addEventListener('mouseleave', onMouseLeaveGrid);
}

// Disable interaction with grid
function ClearEventListeners()
{
    gridView.canvas.removeEventListener('click', OnClickOnGrid);
    gridView.canvas.removeEventListener('mouseenter', onMouseEnterGrid);
    gridView.canvas.removeEventListener('mousemove', onMouseMoveOverGrid);
    gridView.canvas.removeEventListener('mouseleave', onMouseLeaveGrid);
}

function onMouseEnterGrid(/*event*/)
{
}

function onMouseMoveOverGrid(event)
{
    if (isPlayingAnimation)
    {
        return;
    }

    ClearAllTileEffects();

    if (loadedImages.length === 0)
    {
        return;
    }

    HoverTile(event);
}

function onMouseLeaveGrid(/*event*/)
{
    if (isPlayingAnimation)
    {
        return;
    }

    ClearAllTileEffects();

    hoveredCol = -1;
    hoveredRow = -1;
    colToSwap = -1;
    rowToSwap = -1;

    if (loadedImages.length === 0)
    {
        return;
    }

    RenderGrid();
}

function OnClickOnGrid(/*event*/)
{
    //console.log("CLICK");

    if (isPlayingAnimation)
    {
        return;
    }

    RequestSwap(hoveredRow, hoveredCol, rowToSwap, colToSwap);

    hoveredRow = -1;
    hoveredCol = -1;
    rowToSwap = -1;
    colToSwap = -1
}

function HoverTile(event)
{
    let row = Math.floor(event.offsetY / gridViewData.tileSize);
    let col = Math.floor(event.offsetX / gridViewData.tileSize);

    // console.log(row + " / " + col);

    if (AreCoordinatesInGrid(row, col))
    {
        if (hoveredRow !== -1)
            TryToClearTileEffect(GetTileUID(hoveredRow, hoveredCol));

        hoveredRow = row;
        hoveredCol = col;

        SetTileEffect(GetTileUID(hoveredRow, hoveredCol), hoverFilter);
    }

    let dir = FindSwapDirection(event);
    // console.log("Swap with " + dirDebugNames[dir] + " tile");

    let swapName;
    let swapImgXOffset = 0, swapImgYOffset = 0;

    // Determines which are the row and col to swap
    {
        let newRowToSwap = row, newColToSwap = col;

        switch (dir)
        {
            case 0:
                newRowToSwap--;
                swapImgYOffset = -swapFeedbackOffset;
                swapName = "vswap";
                break;
            case 2:
                newRowToSwap++;
                swapImgYOffset = swapFeedbackOffset;
                swapName = "vswap";
                break;
            case 1:
                newColToSwap++;
                swapImgXOffset = swapFeedbackOffset;
                swapName = "hswap";
                break;
            case 3:
                newColToSwap--;
                swapImgXOffset = -swapFeedbackOffset;
                swapName = "hswap";
                break;
        }

        if (!AreCoordinatesInGrid(newRowToSwap, newColToSwap))
        {
            newRowToSwap += (row - newRowToSwap) * 2;
            newColToSwap += (col - newColToSwap) * 2;

            swapImgXOffset *= -1;
            swapImgYOffset *= -1;
        }

        if (AreCoordinatesInGrid(newRowToSwap, newColToSwap))
        {
            if(newRowToSwap !== hoveredRow && newColToSwap !== colToSwap)
                TryToClearTileEffect(GetTileUID(rowToSwap, colToSwap));

            rowToSwap = newRowToSwap;
            colToSwap = newColToSwap;

            SetTileEffect(GetTileUID(rowToSwap, colToSwap), hoverFilter);
        }
    }

    RenderGrid();
    // Render the swap feedback!
    gridView.context.drawImage(
        loadedImages[swapName],
        2 + gridViewData.tileSize * col + swapImgXOffset,
        2 + gridViewData.tileSize * row + swapImgYOffset,
    );
}

function RequestSwap(rowA, colA, rowB, colB)
{
    if (!IsValidSwap(rowA, colA, rowB, colB))
    {
        NotifyInvalidSwap();
        return;
    }

    invalidSwapCount = 0;

    ClearAllTileEffects(true);

    animationQueue.push(new SwapAnimation(rowA, colA, rowB, colB));
    // TODO Predict first alignments and skip the one sent by the server
    StartMainLoop();

    ServerTriggerSwap(rowA, colA, rowB, colB);
}

/* Asynchronous call to request the specified swap */
function ServerTriggerSwap(rowA, colA, rowB, colB)
{
    let payload = {
        r1: rowA,
        c1: colA,
        r2: rowB,
        c2: colB,
    };

    ServerPost('/swapTiles', payload, HandleSwapResponse);
}

function HandleSwapResponse(data)
{
    for (let stepID = 0; stepID < data["steps"].length; stepID++)
    {
        let step = data["steps"][stepID];

        if(!step.hasOwnProperty("alignments"))
        {
            // Step here is the new grid data
            let resetAnimation = new ResetAnimation(step);
            animationQueue.push(resetAnimation);
            continue;
        }

        let alignments = [];
        for (let alignmentID = 0; alignmentID < step["alignments"].length; alignmentID++)
        {
            let currentAlignment = step["alignments"][alignmentID];
            let clearedTiles = [];

            for (let tileID = 0; tileID < currentAlignment["tiles"].length; tileID++)
            {
                clearedTiles.push(new Tile(currentAlignment["tiles"][tileID].row, currentAlignment["tiles"][tileID].col));
            }

            alignments.push(new Alignment(clearedTiles, currentAlignment["element"], currentAlignment["gain"]));
        }

        animationQueue.push(new ClearAnimation(alignments));

        let respawnedTiles = [];
        let spawnsInfo = step["spawns"];
        for (let respawnID = 0; respawnID < spawnsInfo.length; respawnID++)
        {
            respawnedTiles.push(new RespawnInfo(spawnsInfo[respawnID].row, spawnsInfo[respawnID].col, spawnsInfo[respawnID].elem));
        }

        animationQueue.push(new RespawnAnimation(respawnedTiles));

        if(stepID < data["steps"].length - 1)
        {
            animationQueue.push(new WaitingAnimation(waitingTimeBetweenChainSteps));
        }
    }

    animationQueue.push(new ChainReactionEndAnimation(data["resources"]));

    // Don't restart a new main loop if it was already in progress
    if(!isPlayingAnimation)
    {
        StartMainLoop();
    }
}

// Throw an error to the player if he failed to execute a move two times in a row
function NotifyInvalidSwap()
{
    invalidSwapCount++;
    if (invalidSwapCount < 2)
    {
        return;
    }

    invalidSwapCount = 0;

    // Function defined in city.html.twig (to support the translation)
    wrongMessage();
}

//#endregion

//#region Utility

/*
 * Find which direction we should use to with the swap.
 * Direction are (TOP = 0, RIGHT = 1, BOTTOM = 2, LEFT = 3)
 */
function FindSwapDirection(evt)
{
    let dir;

    let x = evt.offsetX % gridViewData.tileSize;
    let y = evt.offsetY % gridViewData.tileSize;
    let halfSize = gridViewData.tileSize * 0.5;
    let xScaled = Math.abs(x - halfSize);
    let yScaled = Math.abs(y - halfSize);

    if (xScaled >= yScaled)
    {
        // 3 : Left, 1 : Right
        dir = (x < halfSize) ? 3 : 1;
    }
    else
    {
        // 0 : Top, 2 : Bottom
        dir = (y < halfSize) ? 0 : 2;
    }

    return dir;
}


/*
 * Triggers a web request with the specified payload and call the callback when request succeeded!
 */
function ServerPost(path, payload, callback)
{
    console.log("[ServerPost] URL:" + cityUrl + path);

    //console.log("[ServerPost] Payload:\n");
    //console.log(payload); // Separated line to be able to manipulate it in the debug console!

    let data = new FormData();
    data.append("json", JSON.stringify(payload));

    fetch(cityUrl + path, {
        method: "POST",
        body  : data
    })
    .then(function (res)
    {
        return res.json();
    })
    .then(function (data)
    {
        console.log("[Server Response]\n" + JSON.stringify(data));
        callback(data)
    })
    .catch(function (error)
    {
        console.error("[Server Error]\n" + error);
    });
}

//#endregion

//#region Maths

function lerp(start, end, t)
{
    return start * (1.0 - t) + end * t;
}

function clamp(number, min, max)
{
    return Math.max(min, Math.min(number, max));
}

// Get random integer between 0 and max
function GetRandomInRange(a_max)
{
    return Math.floor(Math.random() * a_max);
}

//#endregion

//#region Rendering & Animations

function Render(deltaTime)
{
    TickAnimationFrame(deltaTime);
    RenderGrid();
}

function TickAnimationFrame(deltaTime)
{
    animTime += deltaTime;

    switch (currentAnim.id)
    {
        case 0:
            ComputeSwapAnimation();
            break;
        case 1:
            ComputeClearAnimation();
            break;
        case 3:
            ComputeRespawnAnimation();
            break;
        case 2:
            ComputeGravityAnimation();
            break;
        case 4:
            ComputeResetAnimation();
            break;
        case 5:
            ComputeChainReactionEndAnimation();
            break;
        case 6:
            ComputeWaitingAnimation();
            break;
        default:
            console.log("Invalid animation or not implemented (ID: " + currentAnim.id + ")");
            TriggerNextAnimationInQueue();
    }
}

function ComputeSwapAnimation()
{
    // TODO Factorize in TickAnimationFrame?
    let t = clamp(animTime / currentAnim.duration, 0, 1);

    if (t >= 1)
    {
        // UPDATE THE MODEL
        SwapTiles(currentAnim.move1.row, currentAnim.move1.col, currentAnim.move2.row, currentAnim.move2.col);

        // RESTORE THE VIEW
        gridViewData.tilePositions = currentAnim.gridBuffer;

        TriggerNextAnimationInQueue();
    }
    else
    {
        MoveTile(currentAnim.move1.row, currentAnim.move1.col, true, currentAnim.move1.startX, currentAnim.move1.destX, t);
        MoveTile(currentAnim.move1.row, currentAnim.move1.col, false, currentAnim.move1.startY, currentAnim.move1.destY, t);

        MoveTile(currentAnim.move2.row, currentAnim.move2.col, true, currentAnim.move2.startX, currentAnim.move2.destX, t);
        MoveTile(currentAnim.move2.row, currentAnim.move2.col, false, currentAnim.move2.startY, currentAnim.move2.destY, t);
    }
}

function ComputeGravityAnimation()
{
    // Fade tiles then clear them!
    let t = clamp(animTime / currentAnim.duration, 0, 1);

    if (t >= 1)
    {
        // Update model at this gravity step
        for (let respawnID = 0; respawnID < currentAnim.respawnInfos.length; respawnID++)
        {
            let respawnInfo = currentAnim.respawnInfos[respawnID];
            tiles[respawnInfo.row][respawnInfo.col] = respawnInfo.element;
        }

        // RESTORE THE VIEW
        gridViewData.tilePositions = currentAnim.gridBuffer;

        TriggerNextAnimationInQueue();
    }
    else
    {
        ClearRenderView(frontView);

        for (let moveID = 0; moveID < currentAnim.moves.length; moveID++)
        {
            let move = currentAnim.moves[moveID];
            MoveTile(move.row, move.col, true, move.startX, move.destX, t);
            MoveTile(move.row, move.col, false, move.startY, move.destY, t);
        }
    }
}

function ComputeClearAnimation()
{
    // Fade tiles then clear them!
    let t = clamp(animTime / currentAnim.duration, 0, 1);
    let minusT = 1-t;

    if (t < 1)
    {
        ClearRenderView(frontView);

        for (let a = 0; a < currentAnim.alignments.length; a++)
        {
            let averageRow = 0;
            let averageCol = 0;

            for (let i = 0; i < currentAnim.alignments[a].tiles.length; i++)
            {
                let tile = currentAnim.alignments[a].tiles[i];
                SetTileEffect(GetTileUID(tile.row, tile.col), "opacity(" + minusT + ") brightness(" + 1.5 * minusT + ")");

                averageRow += tile.row;
                averageCol += tile.col;
            }

            averageRow /= currentAnim.alignments[a].tiles.length;
            averageCol /= currentAnim.alignments[a].tiles.length;

            DrawAlignmentReward(averageRow, averageCol, currentAnim.alignments[a].element, currentAnim.alignments[a].gain);
        }

        return;
    }

    ClearRenderView(frontView);

    for (let a = 0; a < currentAnim.alignments.length; a++)
    {
        let averageRow = 0;
        let averageCol = 0;

        for (let i = 0; i < currentAnim.alignments[a].tiles.length; i++)
        {
            let tile = currentAnim.alignments[a].tiles[i];
            tiles[tile.row][tile.col] = -1;

            ClearTileEffect(GetTileUID(tile.row, tile.col));

            averageRow += tile.row;
            averageCol += tile.col;
        }

        averageRow /= currentAnim.alignments[a].tiles.length;
        averageCol /= currentAnim.alignments[a].tiles.length;

        DrawAlignmentReward(averageRow, averageCol, currentAnim.alignments[a].element, currentAnim.alignments[a].gain);
    }

    // We have to do it here, else we don't have the proper grid state :(
    CreateGravityAnimationWithCurrentGrid();

    animationQueue.unshift(new WaitingAnimation(waitingTimeBeforeGravity));

    TriggerNextAnimationInQueue();
}

/**
 * Aim: Collect all tile movements and know the state of the model after the application of gravity
 */
function CreateGravityAnimationWithCurrentGrid()
{
    let columns = [];
    for (let a = 0; a < currentAnim.alignments.length; a++)
    {
        for (let i = 0; i < currentAnim.alignments[a].tiles.length; i++)
        {
            let tile = currentAnim.alignments[a].tiles[i];

            if (columns.indexOf(tile.col) === -1)
            {
                columns.push(tile.col);
            }
        }
    }

    let movements = [];
    let respawnInfos = [];

    let distanceMax = gridViewData.tileSize;
    for (let columnId = 0; columnId < columns.length; columnId++)
    {
        let col = columns[columnId];
        let currentEmptyRow = -1;
        let emptyRowCount = 0;
        for (let row = gridSize - 1; row >= 0; row--)
        {
            let elem = tiles[row][col];
            if (elem === -1)
            {
                if (currentEmptyRow === -1)
                {
                    currentEmptyRow = row;
                }

                // We know, which row is empty so we can add one more empty tile on top !!!
                respawnInfos.push(new RespawnInfo(emptyRowCount, col, -1));
                emptyRowCount++;
            }
            else if (elem !== -1 && currentEmptyRow !== -1)
            {
                let move = new Movement(row, col, currentEmptyRow, col);
                movements.push(new Movement(row, col, currentEmptyRow, col));
                respawnInfos.push(new RespawnInfo(currentEmptyRow, col, tiles[row][col]));
                currentEmptyRow--;

                distanceMax = Math.max(distanceMax, move.destY - move.startY);
            }
        }
    }

    if(movements.length === 0)
    {
        return;
    }

    // Increase animation duration depending distance to move
    let GravityAnim = new GravityAnimation(movements, respawnInfos);
    GravityAnim.duration *= Math.max(1, (distanceMax / gridViewData.tileSize) * 0.8);

    // Force animation to play just after!
    animationQueue.unshift(GravityAnim);
}

function ComputeRespawnAnimation()
{
    let t = clamp(animTime / currentAnim.duration, 0, 1);

    if (t < 1)
    {
        for (let i = 0; i < currentAnim.respawnsInfo.length; i++)
        {
            let respawnInfo = currentAnim.respawnsInfo[i];

            // TODO OnStart & OnStop functions for animation class!
            tiles[respawnInfo.row][respawnInfo.col] = respawnInfo.element;
            SetTileEffect(GetTileUID(respawnInfo.row, respawnInfo.col), "opacity(" + t + ")");
        }
        return;
    }

    TriggerNextAnimationInQueue();
}

function ComputeResetAnimation()
{
    let t = clamp((animTime * gridSize*2) / currentAnim.duration, 0, 1);

    // From where do we start?
    let row = 0;
    let col = 0;
    if(currentAnim.diagonal < gridSize)
    {
        row = currentAnim.diagonal;
    }
    else
    {
        row = gridSize-1;
        col = currentAnim.diagonal % gridSize;
    }

    let opacity = currentAnim.reverse ? t : 1-t;
    while(row >= 0 && col < gridSize)
    {
        SetTileEffect(GetTileUID(row, col), "opacity(" + opacity + ")");
        row--;
        col++;
    }

    if(t >= 1)
    {
        animTime = 0;
        currentAnim.reverse ? currentAnim.diagonal-- : currentAnim.diagonal++;

        // Ignore the middle diagonal since else we do it two times
        if(currentAnim.diagonal === gridSize)
            currentAnim.reverse ? currentAnim.diagonal-- : currentAnim.diagonal++;
    }

    if(currentAnim.diagonal === gridSize * 2 - 1)
    {
        currentAnim.reverse = true;
        InitGridWithString(currentAnim.gridData);
    }
    else if(currentAnim.reverse && currentAnim.diagonal === 0)
    {
        TriggerNextAnimationInQueue();
    }
}

function ComputeChainReactionEndAnimation()
{
    // Override all resources with new values sent by the server!
    for(let resourceID in currentAnim.resources)
    {
        let resource = currentAnim.resources[resourceID];
        let newValue = resource['amount'];
        switch (resource['name'])
        {
            case "turn":
                SetTurnCount(newValue);
                break;
            // Resources
            case "wheat":
                SetResourceCount(ResourceType.FOOD, newValue);
                break;
            case "wood":
                SetResourceCount(ResourceType.WOOD, newValue);
                break;
            case "gold":
                SetResourceCount(ResourceType.GOLD, newValue);
                break;
            case "remainingHammers":
                UpdateRemainingHammer(newValue);
                break;
            case "soldier":
                // TODO Increment first army unit
                break;
            // populations
            case "citizen":
                SetPopulationCount(CitizenType.PENDING, newValue);
                break;
            case "merchant":
                SetPopulationCount(CitizenType.MERCHANT, newValue);
                break;
            case "lumberjack":
                SetPopulationCount(CitizenType.LUMBERJACK, newValue);
                break;
            case "recruiter":
                SetPopulationCount(CitizenType.RECRUITER, newValue);
                break;
            case "farmer":
                SetPopulationCount(CitizenType.FARMER, newValue);
                break;
            case "worker":
                SetPopulationCount(CitizenType.WORKER, newValue);
                break;
            default:
        }
    }

    ClearRenderView(frontView);
    ClearAllTileEffects(true);
    EvaluateGrid();

    TriggerNextAnimationInQueue();
}

function ComputeWaitingAnimation()
{
    let t = clamp(animTime / currentAnim.duration, 0, 1);
    if(t >= 1)
    {
        TriggerNextAnimationInQueue();
    }
}

function StartMainLoop()
{
    //console.log("[GAME LOOP] START");
    elapsedTime = 0;
    lastUpdate = Date.now();

    ClearEventListeners();

    TriggerNextAnimationInQueue();

    // Do we need to run the game loop?
    if (isPlayingAnimation)
    {
        Tick();
    }
}

function StopMainLoop()
{
    //console.log("[GAME LOOP] STOP");
    window.cancelAnimationFrame(KingdomLoop.stopMain);

    isPlayingAnimation = false;

    InitEventListeners();
}

function Tick()
{
    KingdomLoop.stopMain = window.requestAnimationFrame(Tick);

    let now = Date.now();
    let deltaTime = now - lastUpdate;
    lastUpdate = now;

    Render(deltaTime);

    elapsedTime += deltaTime;

    //console.log("Tick : " + " dt: " + deltaTime + " - elapsed time: " + elapsedTime);
}

// TODO Rename Trigger NextEventInQueue & animationQueue to EventQueue?
// Call that when an animation ended!
function TriggerNextAnimationInQueue()
{
    animTime = 0;

    currentAnim = animationQueue.shift();
    isPlayingAnimation = currentAnim !== undefined;

    if (!isPlayingAnimation)
    {
        StopMainLoop();
    }
}

function MoveTile(row, col, isX, start, dest, t)
{
    gridViewData.tilePositions[row][col][isX ? 0 : 1] = lerp(start, dest, t);
}

//#endregion

function Initialize()
{
    InitGridView();
    LoadGridDataFromWebPage();

    LoadRelevantCityBuildings(); // Buildings have to be loaded BEFORE resource stocks & populations!
    LoadPopulationFromWebPage();
    LoadResourcesFromWebPage();

    // Load all images once
    let loadImages = async function ()
    {
        for (let key in imagesSrc)
        {
            let img = new Image();
            img.src = "/" + imagesSrc[key];
            await img.decode();
            loadedImages[key] = img;
        }
    }

    loadImages().then(() =>
    {
        RenderGrid();
        EvaluateGrid();
    })

    // TODO Lock grid if city has no turn!

    InitEventListeners();
}

window.addEventListener("load", function ()
{
    Initialize();
});

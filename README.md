EternalKingdom Setup
-------------------------------

EternalKingdom est un projet OpenSource de reconstruction du jeu Kingdom développé par Motion et déployé sur MUXXU.
Il utilise le framework PHP Symfony dans sa version 4.4 !

Credits:
- Lead & Documentation : Drange, Glaurung
- Developpeurs: Talsi, Bibni, Keats, Devwwn et Simpkin.

---

**<a name=RunLocalWebServer> Lancer le serveur WEB en local:</a>**

Afin de pouvoir lancer et tester le site, il faut d'abord procéder à quelques installations.

Rendez vous d'abord sur cette page pour avoir accès à la documentation complète d'un setup Symfony (https://symfony.com/doc/4.4/setup.html)

1. **Technologies à installer** (Technical Requirements)

Veillez à bien installer Php, Composer & Symfony. Et enfin Postgres!
Si la commande `symfony check:requirements` indique Succès, vous pouvez continuer.
Une fois php installé, veuillez modifier le fichier php.ini et décommenter la ligne `extension=pdo_pgsql` (retirer le ';' )

Dernière étape: veillez à modifier vos variables d'environnement.
Copiez le fichier **.env.example** et renommez le **.env.local** puis décommentez et modifiez la ligne DATABASE_URL à votre convenance.

2. **Charger les plugins tierces non stockés sur le git**
Rendez vous dans le dossier EternalKingdom de ce projet git  et lancez la commande:
`composer install`
Ceci à pour but de télécharger et installer les plugins non stockés sur le git du fait de leurs poids.

3. [Optionnel] **Lancement de Eternal Twin** (nécessaire pour l'OAuth2).

Rendez-vous dans le dossier Eternaltwin:  
`yarn install`  
pour installer Eternal Twin et ces dependences  
`yarn etwin`  
Pour lancer l'application 

Vous pouvez maintenant accéder Eternal Twin (lite) depuis http://localhost:50320

4. **Lancement du serveur**
   
lancez le projet Symfony à l'aide de la commande

`symfony server:start` 

Rendez vous maintenant sur http://localhost:8080 et profitez du site 💖

https://eternal-twin.net/docs/app/etwin-oauth

# Configurer Eternaltwin:

Etwin documentation: https://eternal-twin.net/docs/app/etwin-oauth  
dans EternalKingdom/.env: (ou .env.local)
```
    IDENTITY_SERVER_URI="http://localhost:50320"
    OAUTH_CALLBACK="'http://localhost:8000/oauth/callback'" 
    OAUTH_AUTHORIZATION_URI="http://localhost:50320/oauth/authorize"
    OAUTH_TOKEN_URI="http://localhost:50320/oauth/token"
    OAUTH_CLIENT_ID="kingdom@clients"
    OAUTH_SECRET_ID="dev_secret"
```
Ces variables d'environements doivent correspondre a celles definis dans EternalTwin/etwin.toml
```
[clients.kingdom]
display_name = "kingdom"
app_uri = "http://localhost:8000"
callback_uri = "http://localhost:8000/oauth/callback"
secret = "dev_secret"
```

## Utiliser Eternal Twin avec une database (Postgres uniquement)

Etwin documentation: https://eternal-twin.net/docs/app/etwin-integration

Il faut au prealable creer une database pour eternaltwin.
Dans le fichier EternalTwin/etwin.toml changer les parametres suivants pour qu'il corresponde a votre database
```
api = "postgres"
[db]
# Database service host
host = "localhost"
# Database service port
port = 5432
# Database name
name = "eternal_twin"
# Database user (role). In development, it is recommended to be a superuser to manage extensions.
user = "user"
# Password for the database user.
password = "password"
```

Dans le dossier `Eternaltwin` vous pouvez maintenant initialiser la base de données :
```
yarn etwin db create
```

Si vous avez une base de données existante, vous pouvez la mettre à jour avec :

```
yarn etwin db upgrade
```
